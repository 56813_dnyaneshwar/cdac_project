const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()


//-----------------------------------
//      /delivery/add
//-----------------------------------

router.post('/add', (request, response) => {
    if (request.session.user) {
        const userId = request.session.user.id
        const role = request.session.user.role

        const { licenseNo, licenseExp } = request.body

        const statement = `INSERT INTO delivery_man (user_id, license_no, license_exp) VALUES (?,?,?)`

        if (role === 'delivery') {
            const connection = db.openConnection()
            connection.query(statement, [userId, licenseNo, licenseExp], (error, result) => {
                connection.end()
                response.send(utils.createRequest(error, result))
            })
        }
        else {
            response.send(utils.createRequest("User is not a Delivery Man", null))
        }
    } else {
        response.send(utils.createRequest("User Not Found", null))
    }
})

//-----------------------------------
//      /delivery/getDeliveryId
//-----------------------------------

router.get('/getDeliveryId', (request, response) => {
    if (request.session.deliveryId) {
        response.send(utils.createRequest(null, { deliveryId: request.session.deliveryId }))
    } else {
        if (request.session.user) {
            const userId = request.session.user.id
            const statement = `SELECT id FROM delivery_man WHERE user_id = ?`
            const connection = db.openConnection()
            connection.query(statement, [userId], (error, result) => {
                connection.end()
                if (result.length > 0) {
                    request.session.deliveryId = result[0].id
                    response.send(utils.createRequest(error, { deliveryId: result[0].id }))
                }
            })
        } else {
            response.send(utils.createRequest("User Not Found"))
        }
    }
})

//--------------------------------------------
//      /delivery/get
//--------------------------------------------

router.get('/get', (request, response) => {

    var list = []
    var original = []
    const statement = `SELECT * FROM ongoing_delivery WHERE delivery_man_id IS null`

    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (result) {
            original = result
            result.forEach(o => {
                getDetails(o)
            })
        } else {
            response.send(utils.createRequest("Orders for Delivery NOT Found"))
        }
    })
    // Called from 1st API for each record of 1st
    const getDetails = (o) => {
        const statement = `SELECT CONCAT(c.first_name,' ',c.last_name) AS customer_name, CONCAT(a.address_line1, ',', a.address_line2, ',', a.city,',', a.state, ',', a.country, ',', a.postal_code) AS customer_address FROM users c INNER JOIN addresses a ON c.id=a.user_id WHERE c.id = ?`
        const connection = db.openConnection()
        connection.query(statement, [o.customer_id], (error, result) => {
            connection.end()
            if (result) {
                const body = {
                    id: o.id,
                    customerId: o.customer_id,
                    vendorId: o.vendor_id,
                    customerName: result[0].customer_name,
                    customerAddress: result[0].customer_address
                }
                getVendorDetails(body)
            } else {
                console.log('In else getDetails')
            }
        })
    }
    // Called from 2nd API for each record of 1st
    const getVendorDetails = (body) => {
        const statement = `SELECT CONCAT(u.first_name,' ',u.last_name) AS vendor_name, CONCAT(a.address_line1, ',', a.address_line2, ',', a.city,',', a.state, ',', a.country, ',', a.postal_code) AS vendor_address FROM users u INNER JOIN addresses a ON u.id=a.user_id INNER JOIN vendors v ON u.id=v.user_id WHERE v.id = ?`
        const connection = db.openConnection()
        connection.query(statement, [body.vendorId], (error, result) => {
            connection.end()
            if (result) {
                body["vendorName"] = result[0].vendor_name
                body["vendorAddress"] = result[0].vendor_address

                list.push(body)
                if (original.length == list.length) {
                    response.send(utils.createRequest(null, list))
                }
            } else {
                console.log('In else getVendorDetails')
            }
        })
    }

})

//--------------------------------------------
//      /delivery/getPending
//--------------------------------------------

router.get('/getPending', (request, response) => {

    var list = []
    var original = []
    if (request.session.deliveryId) {
        const deliveryId = request.session.deliveryId
        const statement = `SELECT * FROM ongoing_delivery WHERE delivery_man_id = ? AND status='pending'`

        const connection = db.openConnection()
        connection.query(statement, [deliveryId], (error, result) => {
            connection.end()
            if (result) {
                original = result
                result.forEach(o => {
                    getDetails(o)
                })
            } else {
                response.send(utils.createRequest("Orders for Delivery NOT Found"))
            }
        })
        // Called from 1st API for each record of 1st
        const getDetails = (o) => {
            const statement = `SELECT CONCAT(c.first_name,' ',c.last_name) AS customer_name, CONCAT(a.address_line1, ',', a.address_line2, ',', a.city,',', a.state, ',', a.country, ',', a.postal_code) AS customer_address FROM users c INNER JOIN addresses a ON c.id=a.user_id WHERE c.id = ?`
            const connection = db.openConnection()
            connection.query(statement, [o.customer_id], (error, result) => {
                connection.end()
                if (result) {
                    const body = {
                        id: o.id,
                        customerId: o.customer_id,
                        vendorId: o.vendor_id,
                        customerName: result[0].customer_name,
                        customerAddress: result[0].customer_address,
                        productId:o.product_id,
                        productName:o.product_name
                    }
                    getVendorDetails(body)
                } else {
                    console.log('In else getCustomer')
                }
            })
        }
        // Called from 2nd API for each record of 1st
        const getVendorDetails = (body) => {
            const statement = `SELECT CONCAT(u.first_name,' ',u.last_name) AS vendor_name, CONCAT(a.address_line1, ',', a.address_line2, ',', a.city,',', a.state, ',', a.country, ',', a.postal_code) AS vendor_address FROM users u INNER JOIN addresses a ON u.id=a.user_id INNER JOIN vendors v ON u.id=v.user_id WHERE v.id = ?`
            const connection = db.openConnection()
            connection.query(statement, [body.vendorId], (error, result) => {
                connection.end()
                if (result) {
                    console.log(result)
                    body["vendorName"] = result[0].vendor_name
                    body["vendorAddress"] = result[0].vendor_address

                    list.push(body)
                    if (original.length == list.length) {
                        response.send(utils.createRequest(null, list))
                    }
                } else {
                    console.log('In else getCustomer')
                }
            })
        }
    }
})

//--------------------------------------------
//      /delivery/accept
//--------------------------------------------

router.post('/accept', (request, response) => {
    const { ongoingDeliveryId } = request.body
    if (request.session.deliveryId) {
        const deliveryId = request.session.deliveryId
        const statement = `update ongoing_delivery set delivery_man_id = ? where id=?`
        const connection = db.openConnection()
        connection.query(statement, [deliveryId, ongoingDeliveryId], (error, result) => {
            connection.end()
            if (result) {
                response.send(utils.createRequest(null, result))
            } else {
                response.send(utils.createRequest(error, null))
            }
        })
    } else {
        response.send(utils.createRequest("Delivery Man Not Found"))
    }
})

//--------------------------------------------
//      /delivery/delivered
//--------------------------------------------

router.post('/delivered', (request, response) => {
    const { ongoingDeliveryId } = request.body
    if (request.session.deliveryId) {
        const deliveryId = request.session.deliveryId
        const statement = `update ongoing_delivery set status = 'delivered' where id=? AND delivery_man_id = ?`
        const connection = db.openConnection()
        connection.query(statement, [ongoingDeliveryId, deliveryId], (error, result) => {
            connection.end()
            if (result) {
                response.send(utils.createRequest(null, result))
            } else {
                response.send(utils.createRequest(error, null))
            }
        })
    } else {
        response.send(utils.createRequest("Delivery Man Not Found"))
    }
})

module.exports = router