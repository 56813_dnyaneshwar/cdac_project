import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import { useNavigate } from 'react-router';
import PendingOrder from '../../components/PendingOrder';

const PendingDelivery = () => {
    const [deliveryId, setDeliveryId] = useState('')
    const [orders, setOrders] = useState([])
    const navigate = useNavigate()


    axios.defaults.withCredentials = true;

    const getDeliveryHome = () => {
        const url = `${URL}/delivery/getPending`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setOrders(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }


    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/delivery/getDeliveryId`
        axios.get(url).then((response) => {
            const result = response.data
            setDeliveryId(result['data'].deliveryId)
        })
        getDeliveryHome()
    }, [])

    return (
        <div>
            {deliveryId &&
                <div>
                    <div className='NavBar'>
                        <ul className="nav justify-content-end">
                            <li className="Page">Pending Orders</li>
                            <li className="nav-item">
                                <Link to={'/DeliveryHome'} >Delivery Home</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={''} onClick={Logout}>Log Out</Link>
                            </li>
                        </ul>
                    </div>
                    <div>
                        {orders.map((order) => {
                            return <PendingOrder order={order} />
                        })}
                    </div>
                </div> || <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddDelivery'} >Add Delivery Man Details</Link>
                </div>
            }
        </div>
    )
}

export default PendingDelivery