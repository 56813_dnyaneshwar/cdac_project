import './index.css'
import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import 'bootstrap/dist/css/bootstrap.min.css';

const AddAccount = () => {
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [bankName , setBankName] = useState('')
    const [accountHolderName , setAccountHolderName] = useState('')
    const [accountNo , setAccountNo] = useState('')
    const [ifscCode , setIfscCode] = useState('')
    
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const addAccountDetails = ()=>{
        if(userId !== null){
                const body = { userId, bankName, accountHolderName, accountNo, ifscCode }
                const url = `${URL}/account/add`
                axios.post(url,body).then((response)=>{
                    const result = response.data
                    if(result['status'] === 'success'){
                        toast.success('Account Added Successfully')
                        if(role === 'customer')
                                navigate('/CustomerHome')
                            else if(role === 'vendor')
                                navigate('/VendorHome')
                            else if(role === 'delivery')
                                navigate('/DeliveryHome')
                            else
                                navigate('/')
                    } else{
                        toast.error('Invalid')
                    }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(()=>{
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response)=>{
            const result = response.data
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    },[])

    const clearInput = () =>{
        const bankNameInput = document.getElementById('bankName')
        bankNameInput.value = ""
        const accountHolderNameInput = document.getElementById('accountHolderName')
        accountHolderNameInput.value = ""
        const accountNoInput = document.getElementById('accountNo')
        accountNoInput.value = ""
        const ifscCodeInput = document.getElementById('ifscCode')
        ifscCodeInput.value = ""
    
    }

return (
    <div>
        <h1 className="HIRT">HIRT</h1>
        <h1 className='title'>Add Account</h1>
        { userId && <div className="row">
            <div className="col"></div>
            <div className='col'>
            <div className="form">

            <div className="mb-3">
                <label className="label-control">Bank Name</label>
                <input id="bankName" type="text" className="form-control" placeholder="eg. State Bank of India ..., "
                onChange={(e)=>{
                    setBankName(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Account Holder Name</label>
                <input id="accountHolderName" type="text" className="form-control" placeholder="Lorem Ipsum"
                onChange={(e)=>{
                    setAccountHolderName(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Account No</label>
                <input id="accountNo" type="text" className="form-control" placeholder="eg.34516217841"
                onChange={(e)=>{
                    setAccountNo(e.target.value)
                }}
                />
            </div>    

            <div className="mb-3">
                <label className="label-control">IFSC Code</label>
                <input id="ifscCode" type="text" className="form-control" placeholder="eg. SBI0001245"
                onChange={(e)=>{
                    setIfscCode(e.target.value)
                }}
                />
            </div>

                <div className="button">
                    <button className="btn btn-primary"
                    onClick={addAccountDetails}
                    >Add Address</button>
                </div>
                <div className="button">
                    <button className="btn btn-danger"
                    onClick={clearInput}
                    >Clear</button>
                </div>
            </div>
            </div> 
            <div className="col"></div>
            </div>
            ||  <div style={{color:'red', textAlign: 'center'}}>
                    <h1>Please Signin First</h1>
                    <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                </div>
        }
    </div>
)
}

export default AddAccount