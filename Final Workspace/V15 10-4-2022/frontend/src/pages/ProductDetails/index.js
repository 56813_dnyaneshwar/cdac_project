import './index.css'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import { Link } from 'react-router-dom'

const ProductDetails = () => {

    const product = JSON.parse(sessionStorage["product"])
    const navigate = useNavigate()
    axios.defaults.withCredentials = true;


    const vendor_id = product.vendor_id
    const product_id = product.id
    const product_name = product.product_name
    const amount = product.price
    const quantity = 1
    const unit = product.unit
    console.log(product_name)
    const cart = () => {
        const body = {
            vendor_id,
            product_id,
            product_name,
            amount,
            quantity,
            unit,
        }
        const url = `${URL}/cart/addToCart`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('product added successfully')
            } else {
                toast.error('something went wrong! please try again')
            }
        })
    }

    const goToBuy = () => {
        sessionStorage["product"] = JSON.stringify(product)
        navigate('/Buy')
    }

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
          const result = response.data
          if (result['status'] === 'success') {
            navigate('/')
            toast.success(result['data'])
          } else {
            toast.error('Failed to Logout')
          }
        })
      }

    return (
        <div>
            <div className='NavBar'>
                <ul className="nav justify-content-end">
                    <li className="Page">Product Details</li>
                    <li className="nav-item">
                        <Link to={'/Cart'} >Go to Cart</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={'/CustomerHome'} >Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={''} onClick={Logout}>Log Out</Link>
                    </li>
                </ul>
            </div>

            <div className="products">
                <div className="product-card">
                    <div className="product-info">
                        <h5>{product.product_name} ({product.brand_name})</h5>
                        <h6>Stock : {product.stock}</h6>
                        <h6>Max. Shelf Life : {product.max_shelf_life}</h6>
                        <h6>Price : {product.price} Rs / {product.unit}</h6>
                        <div className="form">
                            <button id='button' className='btn btn-danger' onClick={goToBuy}>Buy Now</button>
                            <button id='button' className='btn btn-dark' onClick={cart}>Add to Cart</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    )

}
export default ProductDetails