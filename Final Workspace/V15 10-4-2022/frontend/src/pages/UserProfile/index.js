import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { URL } from '../../config'

const UserProfile = () => {
    const [user, setUser] = useState([])

    axios.defaults.withCredentials = true;
    useEffect(() => {
        const url = `${URL}/vendor/getVendorProfile`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUser(result['data'])
        })
    }, [])

    return (
        <div>
            <div className='NavBar'>
                <ul className="nav justify-content-end">
                    <li className="Page">{user.first_name}'s Profile</li>
                    <li className="nav-item">
                        <Link to={'/VendorHome'} >Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={'/ChangePassword'} >Change Password</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={'/LogOut'} >Log Out</Link>
                    </li>
                </ul>
            </div>
            <div className="row">
                <div className="col">
                </div>
                <div className='col'>
                    <div className="form">
                        <br /><br />
                        <div className="mb-3">
                            <label className="label-control">First Name : {user.first_name}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Last Name : {user.last_name}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Email Id : {user.email}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Phone Number : {user.phone_no}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Role : {user.role}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Owner Name : {user.owner_name}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">Shop Name : {user.shop_name}</label>
                        </div>
                        <div className="mb-3">
                            <label className="label-control">GST No : {user.gst_no}</label>
                        </div>
                    </div>
                </div>
                <div className="col">
                </div>
            </div>
        </div>
    )
}

export default UserProfile