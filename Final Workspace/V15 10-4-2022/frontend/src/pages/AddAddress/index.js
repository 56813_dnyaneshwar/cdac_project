import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'

const AddAddress = () => {
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [addressLine1, setAddressLine1] = useState('')
    const [addressLine2, setAddressLine2] = useState('')
    const [city, setCity] = useState('')
    const [postalCode, setPostalCode] = useState('')
    const [state, setState] = useState('')
    const [country, setCountry] = useState('')
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const addAddressDetails = ()=>{
        if(userId !== null){
            const body = {
                addressLine1,
                addressLine2,
                city,
                postalCode,
                state,
                country
            }
            const url = `${URL}/address/add`
            axios.post(url,body).then((response)=>{
                const result = response.data
                if(result['status'] == 'success'){
                    toast.success('Address Added Successfully')
                    if(role === 'customer')
                        navigate('/CustomerHome')
                    else if(role === 'vendor')
                        navigate('/AddAccount')
                    else if(role === 'delivery')
                        navigate('/DeliveryHome')
                    else
                        navigate('/')
                } else{
                    toast.error('Invalid')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(()=>{
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response)=>{
            const result = response.data
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    },[])

    const clearInput = () =>{
        const addressLine1Input = document.getElementById('addressLine1')
        addressLine1Input.value = ""
        const addressLine2Input = document.getElementById('addressLine2')
        addressLine2Input.value = ""
        const cityInput = document.getElementById('city')
        cityInput.value = ""
        const postalCodeInput = document.getElementById('postalCode')
        postalCodeInput.value = ""
        const stateInput = document.getElementById('state')
        stateInput.value = ""
        const countryInput = document.getElementById('country')
        countryInput.value = ""
    }

return (
    <div>
        <h1 className="HIRT">HIRT</h1>
        { userId && 
        <div>
            <h1 className='title'>Add Shop Address</h1>
            <div className="row">
            <div className="col"></div>
            <div className='col'>
            <div className="form">

            <div className="mb-3">
                <label className="label-control">Address Line 1</label>
                <input id="addressLine1" type="text" className="form-control" placeholder="lorems Colony"
                onChange={(e)=>{
                    setAddressLine1(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Address Line 2</label>
                <input id="addressLine2" type="text" className="form-control" placeholder="eg.Lorem Ipsum school"
                onChange={(e)=>{
                    setAddressLine2(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">City</label>
                <input id="city" type="text" className="form-control" placeholder="eg.Pune, Vasco ..."
                onChange={(e)=>{
                    setCity(e.target.value)
                }}
                />
            </div>    

            <div className="mb-3">
                <label className="label-control">Postal Code</label>
                <input id="postalCode" type="text" className="form-control" placeholder="eg.411038,403801 ..."
                onChange={(e)=>{
                    setPostalCode(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">State</label>
                <input id="state" type="text" className="form-control" placeholder="eg.Maharashtra, Goa ..."
                onChange={(e)=>{
                    setState(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Country</label>
                <input id="country" type="text" className="form-control" placeholder="eg.US, UK, India ..."
                onChange={(e)=>{
                    setCountry(e.target.value)
                }}
                />
            </div>    

                <div className="button">
                    <button className="btn btn-primary"
                    onClick={addAddressDetails}
                    >Add Address</button>
                </div>
                <div className="button">
                    <button className="btn btn-danger"
                    onClick={clearInput}
                    >Clear</button>
                </div>
            </div>
            </div> 
            <div className="col"></div>
        </div>
        </div> ||   <div style={{color:'red', textAlign: 'center'}}>
                        <h1>Please Signin First</h1>
                        <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                    </div>
        }   
    </div>
)
}

export default AddAddress