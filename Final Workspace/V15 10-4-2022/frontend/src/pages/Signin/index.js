import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react'
import { Link } from 'react-router-dom'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'

const Signin = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [user , setUser] = useState()
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;
    
    const signinUser = ()=>{
        const body = {
            email,
            password,
        }
        const url = `${URL}/user/getByEmailPassword`
        axios.post(url,body).then((response)=>{
            const result = response.data
            if(result['status'] === 'success'){
                setUser(result['data'])
                toast.success('welcome')
                if(user.role === "vendor")
                    navigate('/VendorHome')
                else if(user.role === "customer")
                    navigate('/CustomerHome')
                else if(user.role === "delivery")
                    navigate('/DeliveryHome')
                else if(user.role === "admin")
                    navigate('/AdminHome')
            } else{
                toast.error('Invalid Email / Password')
            }
        })
    }

    const clearInput = () =>{
        const emailInput = document.getElementById('email')
        emailInput.value = ""
        const passwordInput = document.getElementById('password')
        passwordInput.value = ""
    }

return (
    <div>
        <h1 className="HIRT">HIRT</h1>
        <h1 className='title'>Sign In</h1>
        
        <div className="row">
            <div className="col"></div>
            <div className='col'>
            <div className="form">
            <div className="mb-3">
                <label className="label-control">Email</label>
                <input id="email" type="email" className="form-control" placeholder="lorem@test.com"
                onChange={(e)=>{
                    setEmail(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Password</label>
                <input id="password" type="password" className="form-control" placeholder="*******"
                onChange={(e)=>{
                    setPassword(e.target.value)
                }}
                />
            </div>
                <div> not Yet signed up? <Link to={'/signup'} >Signup here</Link> </div>
                    <div className="button">
                        <button className="btn btn-primary"
                        onClick={signinUser}
                        >Signin</button>
                    </div>
                    <div className="button">
                        <button className="btn btn-danger"
                        onClick={clearInput}
                        >Clear</button>
                    </div>
                </div>
            </div> 
            <div className="col"></div>
            </div>       
    </div>
)
}

export default Signin