import { BrowserRouter, Route, Routes, Link } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'

import Signin from './pages/Signin'
import Signup from './pages/Signup'
import AddBusiness from './pages/AddBusiness'
import AddAddress from './pages/AddAddress'
import AddAccount from './pages/AddAccount'
import VendorHome from './pages/VendorHome'
import UpdateProduct from './pages/UpdateProduct'
import AddProduct from './pages/AddProduct'
import ChangePassword from './pages/ChangePassword'

import AdminHome from './pages/AdminHome'
import CustomerList from './pages/CustomerList'
import DeliveryList from './pages/DeliveryList'
import VendorList from './pages/VendorList'
import UserProfile from './pages/UserProfile'

import CustomerHome from './pages/CustomerHome'
import CustomerAllProducts from './pages/CustomerAllProducts'
import BuyProduct from './pages/BuyProduct'
import Cart from './pages/Cart'
import ProductDetails from './pages/ProductDetails'

import DeliveryHome from './pages/DeliveryHome'
import PendingDelivery from './pages/PendingDelivery'
import AddDeliveryMan from './pages/AddDeliveryMan'

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Signin />} />
          <Route path="/Signup" element={<Signup />} />
          <Route path="/AddBusiness" element={<AddBusiness />} />
          <Route path="/AddAddress" element={<AddAddress />} />
          <Route path="/AddAccount" element={<AddAccount />} />
          <Route path="/AddProduct" element={<AddProduct />} />
          <Route path="/UpdateProduct" element={<UpdateProduct />} />
          <Route path="/VendorHome" element={<VendorHome />} />
          <Route path="/ChangePassword" element={<ChangePassword />} />

          <Route path="/AdminHome" element={<AdminHome />} />
          <Route path="/customerList" element={<CustomerList />} />
          <Route path="/deliveryList" element={<DeliveryList />} />
          <Route path="/vendorList" element={<VendorList />} />
          <Route path="/UserProfile" element={<UserProfile />} />

          <Route path="/CustomerHome" element={<CustomerHome />} />
          <Route path="/CustomerAllProducts" element={<CustomerAllProducts />} />
          <Route path="/Buy" element={<BuyProduct />} />
          <Route path="/Cart" element={<Cart />} />
          <Route path="/ProductDetails" element={<ProductDetails />} />

          <Route path="/DeliveryHome" element={<DeliveryHome />} />
          <Route path="/PendingDelivery" element={<PendingDelivery />} />
          <Route path="/AddDeliveryMan" element={<AddDeliveryMan />} />

        </Routes>
      </BrowserRouter>

      <ToastContainer
        position="top-right"
        autoClose={800}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss={false}
        draggable
        pauseOnHover={false}
      />
    </div>
  );
}

export default App;
