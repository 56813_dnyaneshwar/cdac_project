import 'bootstrap/dist/css/bootstrap.min.css';
import { toast } from 'react-toastify';
import axios from 'axios';
import { Link } from 'react-router-dom'
import { URL } from '../config'
import './PendingOrder.css'

const PendingOrder = (props) => {
    const { order } = props
    const ongoingDeliveryId = order.id

    axios.defaults.withCredentials = true;

    const orderDelivered = () => {
        const body = {
            ongoingDeliveryId
        }
        const url = `${URL}/delivery/delivered`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                window.location.reload(false);
            } else {
                toast.error('Failed to Deliver')
            }
        })
    }

    return (
        <div id="PendingOrder">
            <div className="card">
                <div className="card-header">
                    Order Id : {order.id} | Product Id : {order.productId} - {order.productName}
                </div>
                <div className="card-body">
                    <h5 className="card-title">To:{order.customerName}</h5>
                    <p className="card-text">Customer Address : {order.customerAddress}</p>
                    <h5 className="card-title">To:{order.vendorName}</h5>
                    <p className="card-text">Customer Address : {order.vendorAddress}</p>
                    <Link to={''} onClick={orderDelivered} className="btn btn-primary">Deliver</Link>
                </div>
            </div>
        </div>
    )
}

export default PendingOrder  