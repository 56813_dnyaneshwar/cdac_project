const userRouter = require('./routes/user')
const vendorRouter = require('./routes/vendor')
const addressRouter = require('./routes/address')
const productRouter = require('./routes/product')
const accountRouter = require('./routes/account')

const adminRouter = require('./routes/admin')

const customerRouter = require('./routes/DDRoutes/customer')
const cartRouter=require('./routes/DDRoutes/cart')
const orderHistory=require('./routes/DDRoutes/orderHistory')

const deliveryRouter = require('./routes/delivery')

const utils = require('./utils')
const express = require('express')
const cors = require('cors')

const session = require("express-session")
const bodyParser = require("body-parser")
const cookieParser = require("cookie-parser")

const app = express()

app.use(express.json())

app.use(
    cors({
      origin: ["http://localhost:3000"],
      methods: ["GET", "POST","DELETE","PUT"],
      credentials: true,
    })
)

app.use(cookieParser())
  
app.use(bodyParser.urlencoded({ extended: true }))

// key reffers to 2 things 1st session object 2nd is cookie key and value is sessionId
// Session  --> session.id = sessionObject  (includes any thing eg. user, vendor)
// Cookie   --> userId = session.id
// how we write session
app.use(
  session({
    key: "HIRT_Project",
    secret: "HIRT_Project_Secret",
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 1000 * 60 * 60 * 24 * 7,  // (maxAge : one week)
    },
  })
)

app.post('/logout', (request, response, next)=>{
  console.log('Logout Called')
  console.log(request.session.user)
  request.session.destroy();
  response.clearCookie('HIRT_Project');
  response.send(utils.createRequest(null,"Logged Out Sucessfully"))
 })

app.use('/user',userRouter)
app.use('/vendor',vendorRouter)
app.use('/address',addressRouter)
app.use('/product',productRouter)
app.use('/account',accountRouter)

app.use('/admin',adminRouter)

app.use('/customer', customerRouter)
app.use('/history',orderHistory);
app.use('/cart',cartRouter)

app.use('/delivery', deliveryRouter)

app.listen(4000,()=>{
    console.log('Server Started on Port 4000')
})