import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import ListCart from '../../components/listCart'
import { Link } from 'react-router-dom'

const Cart = () => {
    const [cart, setCart] = useState([])
    const [total, setTotal] = useState('')
    axios.defaults.withCredentials = true;
    const navigate = useNavigate()

    const clearCart = () => {
        const url = `${URL}/cart/emptyAddToCart`
        axios.delete(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('cart deleted successfully')
                window.location.reload(false);
            }
            else
                toast.error('something went wrong! please try again')
        })
    }

    const buyAll = () => {
        const url = `${URL}/customer/buyAll`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('success')
                navigate('/CustomerHome')
            }
            else
                toast.error('something went wrong! please try again')
        })
    }

    useEffect(() => {
        const url = `${URL}/cart/getCart`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setCart(result['data'].data)
                console.log(result['data'].sum)
                setTotal(result['data'].sum)
            }
            else
                toast.error('could not fetch Cart! please try again')
        })
        console.log('getting called')
    }, [])

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
          const result = response.data
          if (result['status'] === 'success') {
            navigate('/')
            toast.success(result['data'])
          } else {
            toast.error('Failed to Logout')
          }
        })
      }

    return (
        <div>
            <div className='NavBar'>
                <ul className="nav justify-content-end">
                    <li className="Page">Cart</li>
                    <li className="nav-item">
                        <Link to={'/CustomerHome'} >Home</Link>
                    </li>
                    <li className="nav-item">
                        <Link to={''} onClick={Logout}>Log Out</Link>
                    </li>
                </ul>
            </div>

            {cart && cart.map((cart) => {
                return <ListCart cart={cart} />
            })}
            <div id='total'></div>
            <div className="text-center" ><h4>Total Amount : {total}</h4></div>
            <div className="text-center">
                <button id='button' className="btn btn-success" onClick={buyAll}>Buy All</button>
                <button id='button' className="btn btn-danger" onClick={clearCart}>Clear Cart</button>
            </div>

        </div>
    )
}

export default Cart