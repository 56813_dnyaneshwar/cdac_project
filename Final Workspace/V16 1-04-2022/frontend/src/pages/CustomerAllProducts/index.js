import './index.css'
import Product from '../../components/CustomerProducts'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../../config'
import { Link } from 'react-router-dom'

const CustomerAllProducts = () => {
  const [allProducts, setAllProducts] = useState([])
  const navigate = useNavigate()

  axios.defaults.withCredentials = true;

  useEffect(() => {
    const url = `${URL}/customer/getAllProducts`
    axios.get(url).then((response) => {
      const result = response.data
      setAllProducts(result['data'])
    })
    console.log('getting called use effect')
  }, [])

  const Logout = () => {
    const url = `${URL}/logout`
    axios.post(url).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        navigate('/')
        toast.success(result['data'])
      } else {
        toast.error('Failed to Logout')
      }
    })
  }

  return (
    <div>
      <div className='NavBar'>
        <ul className="nav justify-content-end">
          <li className="Page">All Products</li>
          <li className="nav-item">
            <Link to={'/Cart'} >Go to Cart</Link>
          </li>
          <li className="nav-item">
            <Link to={'/CustomerHome'} >Home</Link>
          </li>
          <li className="nav-item">
            <Link to={''} onClick={Logout}>Log Out</Link>
          </li>
        </ul>
      </div>

      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {allProducts && allProducts.map((product) => {
            return <Product product={product} />
          })}
        </div>
      </div>
    </div>
  )
}

export default CustomerAllProducts