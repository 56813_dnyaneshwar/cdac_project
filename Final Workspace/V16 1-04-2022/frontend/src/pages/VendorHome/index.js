import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import Product from '../../components/Product';
import { useNavigate } from 'react-router';

const VendorHome = () => {
    const [vendorId, setVendorId] = useState('')
    const [products, setProducts] = useState([])
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const getVendorHome = () => {
        const url = `${URL}/product/getAll`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setProducts(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }


    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/vendor/getVendorId`
        axios.get(url).then((response) => {
            const result = response.data
            setVendorId(result['data'].vendorId)
        })
        getVendorHome()
    }, [])

    return (
        <div>
            {vendorId &&
                <div>
                    <div className='NavBar'>
                        <ul className="nav justify-content-end">
                            <li className="Page">Vendor's Home</li>
                            <li className="nav-item">
                                <Link to={'/AddProduct'} >Add Product</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={'/VendorProfile'} >My Profile</Link>
                            </li>
                            <li className="nav-item">
                                <Link to={''} onClick={Logout}>Log Out</Link>
                            </li>
                        </ul>
                    </div>
                    <div>
                        {products.map((product) => {
                            return <Product product={product} />
                        })}
                    </div>
                </div> || <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddBusiness'} >Add Business Details</Link>
                </div>
            }
        </div>
    )
}

export default VendorHome