import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'

const Signin = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [user, setUser] = useState()
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const signinUser = () => {
        const body = {
            email,
            password,
        }
        const url = `${URL}/user/getByEmailPassword`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setUser(result['data'])
                toast.success('welcome')
                if (user.role === "vendor")
                    navigate('/VendorHome')
                else if (user.role === "customer")
                    navigate('/CustomerHome')
                else if (user.role === "delivery")
                    navigate('/DeliveryHome')
                else if (user.role === "admin")
                    navigate('/AdminHome')
            } else {
                toast.error('Invalid Email / Password')
            }
        })
    }

    const clearInput = () => {
        const emailInput = document.getElementById('email')
        emailInput.value = ""
        const passwordInput = document.getElementById('password')
        passwordInput.value = ""
    }

    return (
        <div>
            <div>
                {/*Main Navigation*/}
                <header>
                    <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image3.jpg);height: 100vh;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                    {/* Navbar */}
                    <nav className="navbar navbar-expand-lg navbar-dark d-none d-lg-block" style={{ zIndex: 2000 }}>
                        <div className="container-fluid">
                            {/* Navbar brand */}
                            <a className="navbar-brand nav-link" target="_blank" href="http://localhost:3000/">
                                <strong>HIRT</strong>
                            </a>
                            <button className="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarExample01" aria-controls="navbarExample01" aria-expanded="false" aria-label="Toggle navigation">
                                <i className="fas fa-bars" />
                            </button>
                            <div className="collapse navbar-collapse" id="navbarExample01">
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    <li className="nav-item active">
                                        <a className="nav-link" aria-current="page" href="/">Home</a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link" href="/signup" rel="nofollow" target="_blank">Sign Up</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                    {/* Navbar */}
                    {/* Background image */}
                    <div id="intro" className="bg-image shadow-2-strong">
                        <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}>
                            <div className="container">
                                <div className="row justify-content-center">
                                    <div className="col-xl-5 col-md-8">
                                        <div className="bg-white  rounded-5 shadow-5-strong p-5">
                                            {/* Email input */}
                                            <div className="form-outline mb-4">
                                                <h4 style={{ textAlign: 'center' }}>Sign In</h4>
                                                <label className="form-label">Email address</label>
                                                <input type="email" id="form1Example1" className="form-control" placeholder="lorem@test.com"
                                                    onChange={(e) => {
                                                        setEmail(e.target.value)
                                                    }}
                                                />
                                            </div>
                                            {/* Password input */}
                                            <div className="form-outline mb-4">
                                                <label className="form-label">Password</label>
                                                <input type="password" id="form1Example1" className="form-control" placeholder="lorem@test.com"
                                                    onChange={(e) => {
                                                        setPassword(e.target.value)
                                                    }}
                                                />
                                            </div>
                                            {/* 2 column grid layout for inline styling */}
                                            {/* <div className="row mb-4">
                                                <div className="col d-flex justify-content-center">
                                                    <div className="form-check">
                                                        <input className="form-check-input" type="checkbox" defaultValue id="form1Example3" defaultChecked />
                                                        <label className="form-check-label">
                                                            Remember me
                                                        </label>
                                                    </div>
                                                </div>
                                                <div className="col text-center">
                                                    <a href="#!">Forgot password?</a>
                                                </div>
                                            </div> */}
                                            {/* Submit button */}
                                            <button className="btn btn-primary btn-block"
                                                onClick={signinUser}
                                            >Signin</button>
                                            <button className="btn btn-danger"
                                                onClick={clearInput}
                                            >Clear</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Background image */}
                </header>
                {/*Main Navigation*/}
                {/*Footer*/}
                <footer className="bg-light text-lg-start">
                    {/* Copyright */}
                    <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
                        © 2022 Copyright:
                        <a className="text-dark" href="http://localhost:3000/">HIRT_Project_Sunbeam</a>
                    </div>
                    {/* Copyright */}
                </footer>
                {/*Footer*/}
            </div>
        </div>
    )
}

export default Signin