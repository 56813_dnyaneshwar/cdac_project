import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import 'bootstrap/dist/css/bootstrap.min.css';
import { GiFruitBowl } from 'react-icons/gi'

const AddDeliveryMan = () => {
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [licenseNo, setLicenseNo] = useState('')
    const [licenseExp, setLicenseExp] = useState('')

    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const addDeliveryDetails = () => {
        if (userId !== null) {
            const body = {
                licenseNo,
                licenseExp
            }
            const url = `${URL}/delivery/add`
            axios.post(url, body).then((response) => {
                const result = response.data
                if (result['status'] == 'success') {
                    toast.success('Delivery Man Added Successfully')
                    navigate('/AddAddress')
                } else {
                    toast.error('Invalid')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(() => {
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    }, [])

    const clearInput = () => {
        document.getElementById('licenseNo').value = ''
        document.getElementById('licenseExp').value = ''
    }

    return (
        <div>
            <span className="HIRT">HIRT</span>
            {userId && role === 'delivery' &&
                <div className="row">
                    <h1 className='title'>Delivery Man Details</h1>
                    <div className="col"></div>
                    <div className='col'>
                        <div className="form">
                            <div className="mb-3">
                                <label className="label-control">License Number</label>
                                <input id="licenseNo" type="text" className="form-control" placeholder="MH19ED234355"
                                    onChange={(e) => {
                                        setLicenseNo(e.target.value)
                                    }}
                                />
                            </div>
                            <div className="mb-3">
                                <label className="label-control">License Expiry</label>
                                <input id="licenseNo" type="text" className="form-control" placeholder="DD-MM-YYYY"
                                    onChange={(e) => {
                                        setLicenseExp(e.target.value)
                                    }}
                                />
                            </div>
                            <div className="button">
                                <button className="btn btn-primary"
                                    onClick={addDeliveryDetails}
                                >Add Details</button>
                            </div>
                            <div className="button">
                                <button className="btn btn-danger"
                                    onClick={clearInput}
                                >Clear</button>
                            </div>
                        </div>
                    </div>
                    <div className="col"></div>
                </div>

                || <div style={{ color: 'red', textAlign: 'center' }}>
                    <h1>Sorry You Are Not Delivery Man !!!</h1>
                    <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                </div>


            }
        </div>
    )
}

export default AddDeliveryMan