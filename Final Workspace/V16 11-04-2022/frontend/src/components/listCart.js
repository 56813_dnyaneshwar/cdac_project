import { useEffect } from "react"

const ListCart = (props) => {
    const { cart } = props
    useEffect(() => {
        console.log("in ListCart")
        console.log(cart)
    }, [])
    return (
        <div className="products">
            <div className="product-card">
                <div className="product-info">
                    <h5>{cart.product_name}</h5>
                    <h6>Amount : {cart.amount}</h6>
                    <h6>Quantity : {cart.quantity}</h6>
                </div>
            </div>
        </div>
    )
}
export default ListCart