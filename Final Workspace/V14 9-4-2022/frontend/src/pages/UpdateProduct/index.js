import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify'
import axios from 'axios'
import { useLocation, useNavigate } from 'react-router'
import { URL } from '../../config'

const UpdateProduct = () => {
    const productId = sessionStorage['productId']
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [paymentMethod, setPaymentMethod] = useState(1)
    const [maxShelfLife, setMaxShelfLife] = useState('')
    const [moreDetails, setMoreDetails] = useState('')
    const [price, setPrice] = useState('')
    const [unit, setUnit] = useState('')
    const [stock, setStock] = useState('')

    const navigate = useNavigate()


    axios.defaults.withCredentials = true;

    const updateProductDetails = () => {
        if (userId !== null) {
            const body = { paymentMethod, maxShelfLife, moreDetails, price, unit, stock, productId }
            const url = `${URL}/product/update`
            axios.put(url, body).then((response) => {
                const result = response.data
                if (result['status'] == 'success') {
                    toast.success('Product Updated Successfully')
                    sessionStorage.clear()
                    navigate('/VendorHome')
                } else {
                    toast.error('Invalid Input')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(() => {
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    }, [])

    const clearInput = () => {

        const PaymentMethodInput = document.getElementById('PaymentMethod')
        PaymentMethodInput.value = ""
        const MaxShelfLifeInput = document.getElementById('MaxShelfLife')
        MaxShelfLifeInput.value = ""
        const MoreDetailsInput = document.getElementById('MoreDetails')
        MoreDetailsInput.value = ""
        const PriceInput = document.getElementById('Price')
        PriceInput.value = ""
        const UnitInput = document.getElementById('Unit')
        UnitInput.value = ""
        const StockInput = document.getElementById('Stock')
        StockInput.value = ""
    }

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <div>
            <div className='NavBar'>
                <ul className="nav justify-content-end">
                    <li className="Page">Update Product</li>
                    <li className="nav-item">
                        <Link to={'/VendorHome'} >Home</Link>
                    </li>
                    <li className="nav-item">
                    <Link to={''} onClick={Logout}>Log Out</Link>
                    </li>
                </ul>
            </div>

            {userId && role === 'vendor' &&
                <div className="row">
                    <h1 className='title'>Update Product</h1>
                    <div className="col"></div>
                    <div className='col'>
                        <div className="form">

                            <label className="label-control">PaymentMethod</label>
                            <div style={{ textAlign: 'center' }}>
                                <label className='form-check-label'>COD : </label>
                                <input className='form-check-input'
                                    type="radio"
                                    checked={paymentMethod === "0"}
                                    value="0"
                                    onChange={(e) => setPaymentMethod(e.target.value)} />
                            </div>
                            <div style={{ textAlign: 'center' }}>
                                <label className='form-check-label'>Online : </label>
                                <input className='form-check-input'
                                    type="radio"
                                    checked={paymentMethod === "1"}
                                    value="1"
                                    onChange={(e) => setPaymentMethod(e.target.value)} />
                            </div>

                            <div className="mb-3">
                                <label className="label-control">Maximum Shelf Life</label>
                                <input id="MaxShelfLife" type="number" className="form-control" placeholder="eg.GSTIN00215485"
                                    onChange={(e) => {
                                        setMaxShelfLife(e.target.value)
                                    }}
                                />
                            </div>
                            <div className="mb-3">
                                <label className="label-control">More Details</label>
                                <input id="MoreDetails" type="text" className="form-control" placeholder="This is ..."
                                    onChange={(e) => {
                                        setMoreDetails(e.target.value)
                                    }}
                                />
                            </div>

                            <div className="mb-3">
                                <label className="label-control">Price</label>
                                <input id="Price" type="number" className="form-control" placeholder="eg.50rs, 120rs, ..."
                                    onChange={(e) => {
                                        setPrice(e.target.value)
                                    }}
                                />
                            </div>

                            <div className="mb-3">
                                <label className="label-control">Unit</label>
                                <input id="Unit" type="text" className="form-control" placeholder="eg. kg, g, l, ml, piece, etc"
                                    onChange={(e) => {
                                        setUnit(e.target.value)
                                    }}
                                />
                            </div>
                            <div className="mb-3">
                                <label className="label-control">Stock</label>
                                <input id="Stock" type="number" className="form-control" placeholder="50, 100, 150, ..."
                                    onChange={(e) => {
                                        setStock(e.target.value)
                                    }}
                                />
                            </div>
                            <div className='text-center'>
                                <button className="btn btn-primary" onClick={updateProductDetails} >Update Product</button>
                                <button className="btn btn-danger" onClick={clearInput} >Clear</button>
                            </div>
                        </div>
                    </div>
                    <div className="col"></div>
                </div>

                || <div style={{ color: 'red', textAlign: 'center' }}>
                    <h1>Sorry You Are Not Vendor !!!</h1>
                </div>


            }
        </div>
    )
}

export default UpdateProduct