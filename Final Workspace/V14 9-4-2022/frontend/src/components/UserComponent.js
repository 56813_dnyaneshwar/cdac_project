import './Product.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { toast } from 'react-toastify';
import axios from 'axios';
import { URL } from '../config'

const UserComponent = (props) => {
    const { u } = props
    const id = u.id

    axios.defaults.withCredentials = true;

    const deleteCustomer = () => {
        const body = { id }
        const url = `${URL}/admin/delete`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                window.location.reload(false);
            } else {
                toast.error('Failed to Delete')
            }
        })
    }

    return (
        <div className="products">
            <div className ="product-card">
                <div className="product-info">
                    <h5>{u.first_name} {u.last_name}</h5>
                    <h6>Email : {u.email}</h6>
                    <h6>Phone No. : {u.phone_no}</h6>
                    <h6>Is Active : {u.is_active}</h6>
                    <div className="form">
                        <button className='btn btn-danger' 
                        onClick={deleteCustomer}>Delete</button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default UserComponent