import { useNavigate } from 'react-router'

const Product = (props) => {
    const { product } = props
    const navigate = useNavigate()

    const goToBuy = () => {
        sessionStorage["product"] = JSON.stringify(product)
        navigate('/Buy')
    }
    const moreDetails = () => {
        sessionStorage["product"] = JSON.stringify(product)
        navigate('/ProductDetails')
    }

    return (
        <div className="products">
            <div className="product-card">
                <div className="product-info">
                    <h5>{product.product_name} ({product.brand_name})</h5>
                    <h6>Price : {product.price} Rs / {product.unit}</h6>
                    <div className="form">
                        <button id='button' className='btn btn-danger' onClick={goToBuy}>Buy Now</button>
                        <button id='button' className='btn btn-dark' onClick={moreDetails} >More Details</button>
                    </div>
                </div>
            </div>
        </div>
    )
}
export default Product