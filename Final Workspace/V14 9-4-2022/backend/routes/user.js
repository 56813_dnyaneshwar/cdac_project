const express = require('express')
const bcrypt = require("bcrypt")
const db = require('../db')
const utils = require('../utils')


const router = express.Router()

const saltRounds = 10

//------------------------------
//      /user/add
//------------------------------
router.post('/add',(request,response)=>{
    const { firstName,lastName,email,password,phoneNo,role } = request.body
    const connection = db.openConnection()
    let isActive = 0;
    
    if(role == 'customer')
    isActive = 1;
    
    const checkStatement = `SELECT email FROM users WHERE email = ?`

    connection.query(checkStatement,[email],(error,emails)=>{
        if(error){
            connection.end()
            response.send(utils.createRequest(error))
        } 
        else if(emails.length == 0){
                const insertStatement = `
                INSERT INTO
                users
                (first_name,last_name,email,password,phone_no,role,is_active)
                VALUES
                (? , ? , ? , ? , ? , ?, ?)
                `
                bcrypt.hash(password,saltRounds,(error,bcryptPassword)=>{
                    if(error)
                    response.send(error)
                    else{
                        connection.query(insertStatement,[firstName,lastName,email,bcryptPassword,phoneNo,role,isActive],(error,result)=>{
                        connection.end()
                        response.send(utils.createRequest(error))
                        })
                    }
                })
        } 
        else {
            connection.end()
            error = "User Already Exist"
            response.send(utils.createRequest(error))
        }
    })
})

//------------------------------------
//      /user/getByEmailPassword
//------------------------------------
router.post('/getByEmailPassword',(request,response)=>{
    const { email , password } = request.body
    
    const statement = `SELECT * FROM users WHERE email = ?`

    const connection = db.openConnection()
    
    connection.query(statement,[email,password],(error1,result)=>{
        connection.end()
        if(error1)
            response.send(utils.createRequest(error1))
        else if(result.length == 0){
            error1 = "User not Found"
            response.send(utils.createRequest(error1))
        }
        else {
            bcrypt.compare(password,result[0].password,(error2,resp)=>{
                if (resp) {
                    request.session.user = result[0];                   // adding user in session here
                    result = {
                                userId : result[0].id,
                                firstName : result[0].first_name,
                                role : result[0].role
                            }
                    response.send(utils.createRequest(null,result));
                 } else{
                    error2 = "Invalid Email/Password"
                    response.send(utils.createRequest(error2))
                }
            })
        }
    })
})

//------------------------------
//      /user/getUserIdRole
//------------------------------
router.get('/getUserIdRole',(request,response)=>{
    if(request.session.user){
        const result = {
                            userId : request.session.user.id , 
                            role : request.session.user.role
                        }
        response.send(utils.createRequest(null,result))
    } else {
        const error = "User Not Found"
        response.send(utils.createRequest(error))
    } 
})

//------------------------------
//      /user/update
//------------------------------
router.put('/update',(request,response)=>{
    const { oldPassword, newPassword } = request.body

    if(request.session.user){
// 1st argument ->normal text Password to be compared
// 2nd argument ->encrypted Password
    bcrypt.compare(oldPassword, request.session.user.password, (error1,resp)=>{
            if (resp) {
                const updateStatement = `UPDATE users SET password = ? WHERE id = ?`
                const connection = db.openConnection()
                bcrypt.hash(newPassword, saltRounds, (error2,bcryptPassword)=>{
                    if(error2)
                response.send(utils.createRequest(error2))
                    else{
                        connection.query(updateStatement,[bcryptPassword, request.session.user.id ],(error3,result)=>{
                        connection.end()
                        response.send(utils.createRequest(error3,result))
                        })
                    }
                })
            } else{
                error1 = "Invalid Old Password"
                response.send(utils.createRequest(error1))
            }
        }) 
    } else {
        const error = "User Not Found"
        response.send(utils.createRequest(error))
    } 
    
})
//------------------------------
//      /user/delete
//------------------------------
router.delete('/delete',(request,response)=>{
    const { userId } = request.body
    
    const updateStatement = `DELETE FROM users WHERE id = ?`

    const connection = db.openConnection()
    
    connection.query(updateStatement,[userId ],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

module.exports = router

        // signup user

// { 
//     "firstName" : "Jeet",
//     "lastName" : "Pahari",
//     "email" : "jeet@test.com",
//     "password" : "1234",
//     "phoneNo" : 7499153589,
//     "role" : "admin"  
// }

        // signin user
// {
//     "email" : "jeet@test.com",
//     "password" : "1234"
// }

        // Add Address
// { 
//     "regId" : 1,
//     "addressLine1" : "Vasco Da Gama",
//     "addressLine2" : "near red light pickup",
//     "city" : "Vasco",
//     "postalCode" : 403802,
//     "state" : "Goa",
//     "country" : "India"  
// }

        //Add Account
        
// { 
//     "reg_id" : 1,
//     "bankName" : "State Bank of India",
//     "accountHolderName" : "Jeet Pahari",
//     "accountNo" : "ABC0002154",
//     "ifscCode" : "SBI0009512"
// }