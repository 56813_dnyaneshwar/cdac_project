const express = require("express");
const db = require("../../db");
const utils = require("../../utils");

const router = express.Router();

router.post("/addToCart", (request, response) => {
  const { vendor_id, product_id, product_name, amount, quantity, unit } =
    request.body;
  if (request.session.user) {
    const statement = `INSERT INTO add_to_cart(customer_id,vendor_id,product_id,product_name,amount,quantity,unit) VALUES(?,?,?,?,?,?,?)`;
    const connection = db.openConnection();
    connection.query(
      statement,
      [
        request.session.user.id,
        vendor_id,
        product_id,
        product_name,
        amount,
        quantity,
        unit,
      ],
      (error, data) => {
        connection.end();
        response.send(utils.createRequest(error, data));
      }
    );
  } else {
    response.send(utils.createRequest("something went wrong", null));
  }
});

router.get("/getCart", (request, response) => {
  var sum=0
  if (request.session.user) {
    const statement = `SELECT * FROM add_to_cart WHERE customer_id=${request.session.user.id}`;
    const connection = db.openConnection();
    connection.query(statement, (error, data) => {
      connection.end();
      if (data) {
        for (var i = 0; i < data.length; i++) {
          sum = sum + data[i].amount;
        }
        var data = {
          data: data, sum:sum
        }
        response.send(utils.createRequest(error, data));
      }
    });
  } else {
    response.send(utils.createRequest("something went wrong", null));
  }
});

router.delete("/emptyAddToCart", (request, response) => {
  if (request.session.user.id) {
    const statement = `DELETE FROM add_to_cart WHERE customer_id =${request.session.user.id}`;
    const connection = db.openConnection();
    connection.query(statement, (error, data) => {
      connection.end();
      response.send(utils.createRequest(error, data));
    });
  } else {
    response.send(utils.createRequest("something went wrong", null));
  }
});

router.put("/updateAddToCart", (request, response) => {
  const { id, quantity, amount } = request.body;
  if (request.session.user.id) {
    if (quantity > 0) {
      const statement = `UPDATE add_to_cart SET quantity=?,amount=? where id=?`;
      const connection = db.openConnection();
      connection.query(statement, [quantity, amount, id], (error, data) => {
        connection.end();
        response.send(utils.createRequest(error, data));
      });
    } else {
      const statement = "DELETE FROM add_to_cart WHERE id = ?";
      const connection = db.openConnection();
      connection.query(statement, [id], (error, data) => {
        connection.end();
        response.send(utils.createRequest(error, data));
      });
    }
  } else {
    response.send(utils.createRequest("something went wrong", null));
  }
});

module.exports = router;
