const express = require("express");
const db = require("../../db");
const utils = require("../../utils");

const router = express.Router();

router.get('/getOrderHistory', (request, response) => {
    const {start, end} = request.body
    if (request.session.user.id) {
        const statement = `SELECT * FROM order_history WHERE id=${request.session.user.id} limit ${start},${end}`
        const connection = db.openConnection()
        connection.query(statement, (error, data) => {
            connection.end()
            response.send(utils.showResult(error, data))
        })
    } else {
        response.send(utils.showResult('something went wrong', null))
    }
})

router.post("/addOrderHistory", (request, response) => {
    if (request.session.user.id) {
      var customerId= request.session.user.id

      const sel=`SELECT customer_id,vendor_id,product_id,product_name,amount,quantity,unit FROM ongoing_delivery where customer_id=${customerId}`;
      const connection = db.openConnection();
      let r;
      connection.query(sel,(error,result)=>{
        if(result){
          r=result.map(a=>{
            return Object.values(a);
          })
          callme(r)
        }
        else{
          connection.end();
          response.send(utils.createRequest(error, result));
        }
      })

      const callme=(abc)=>{

        const statement = `INSERT INTO order_history (customer_id,vendor_id,product_id,product_name,amount,quantity,unit) VALUES ?`;

        connection.query(
          statement,[abc],(error, result) => {
          if(result){
            deleteme()
          }else{
            connection.end();
            response.send(utils.createRequest(error, result));
          }
        }
      );
      }
      const deleteme=()=>{
        const statement = `DELETE FROM ongoing_delivery WHERE customer_id =${request.session.user.id}`;
        connection.query(statement,(error,result)=>{
          connection.end();
            response.send(utils.createRequest(error, result));
        })
      }
    } else {
      response.send(utils.createRequest("something went wrong"));
    }
  });


router.put('/updateOngoingDeliveryStatus', (request, response) => {
    const {status} = request.body
    if (request.session.user.id) {
        const statement = `UPDATE ongoing_delivery SET status='${status}' where id=${request.session.user.id}`
        const connection = db.openConnection()
        connection.query(statement, (error, data) => {
            connection.end()
            response.send(utils.showResult(error, data))
        })
    } else {
        response.send(utils.showResult('something went wrong', null))
    }
})

module.exports = router;
