const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//-----------------------------------
//      /address/add
//-----------------------------------
router.post('/add',(request,response)=>{
    const userId = request.session.user.id

    const { addressLine1, addressLine2, city, postalCode, state, country } = request.body

    if(userId !== null){
        const statement = `INSERT INTO addresses (user_id, address_line1, address_line2, city, postal_code, state, country) VALUES (?, ?, ?, ?, ?, ?, ?)`
        const connection = db.openConnection()
        connection.query(statement,[userId, addressLine1, addressLine2, city, postalCode, state, country],(error,result)=>{
            connection.end()
            response.send(utils.createRequest(error,result))
        })
    } else {
        const error = "User Not Found"
        response.send(utils.createRequest(error))
    }
})

//-----------------------------------
//      /address/get
//-----------------------------------
router.get('/get',(request,response)=>{
    if(request.session.user){
        const userId = request.session.user.id
        
        const statement = `SELECT * FROM addresses WHERE user_id = ?`
    
        const connection = db.openConnection()
        
        connection.query(statement,[userId],(error,result)=>{
            connection.end()
            response.send(utils.createRequest(error,result))
        })
    } else{
        response.send(utils.createRequest("User not Found",null))
    }
})

//-----------------------------------
//      /address/update
//-----------------------------------
router.put('/update',(request,response)=>{
    const userId = request.session.user.id
    const { addressLine1, addressLine2, city, postalCode, state, country} = request.body
    
    const statement = `
    Update addresses SET address_line1 = ?, address_line2 = ?, city = ?, postal_code = ?, state = ?, country = ? WHERE user_id = ?
    `

    const connection = db.openConnection()
    
    connection.query(statement,[addressLine1, addressLine2, city, postalCode, state, country ,userId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})

//-----------------------------------
//      /address/delete
//-----------------------------------
router.delete('/delete',(request,response)=>{
    const userId = request.session.user.id
    
    const statement = `DELETE FROM addresses WHERE user_id = ?`

    const connection = db.openConnection()
    
    connection.query(statement,[userId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

module.exports = router