table
    srNo,
    orderNo,
    customerName,
    Product,
    Quantity

CREATE VIEW vendorHome AS
SELECT 
o.order_id, 
CONCAT(c.first_name,c.last_name) AS customer_name,
o.product_name,
o.quantity,
o.status
FROM order_history o
INNER JOIN registration c 
ON 
c.id=o.customer_id;