const mysql = require('mysql')

const openConnection=()=>{
    const connection = mysql.createConnection({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'project_hirt_express'
    })
    connection.connect()
    return connection
}

module.exports = {
    openConnection,
}