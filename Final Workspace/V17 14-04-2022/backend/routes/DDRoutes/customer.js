const express = require("express");
const db = require("../../db");
const utils = require("../../utils");
const router = express.Router();

//--------------------------------------------------------
//      get all products
//--------------------------------------------------------
router.get("/getAllProducts", (request, response) => {
  if (request.session.user) {
    statement = `SELECT * FROM products`;
    const connection = db.openConnection();
    connection.query(statement, (error, data) => {
      connection.end();
      response.send(utils.createRequest(error, data));
    });
  } else {
    response.send(utils.createRequest("Can't get all Products", null));
  }
});

//--------------------------------------------------------
//      get products by type, productName, brandName
//--------------------------------------------------------

router.post("/search/byCategory", (request, response) => {
  const { type, productName, brandName } = request.body;

  if (request.session.user) {
    let statement;
    if (brandName != null && productName != null) {
      statement = `SELECT * FROM products WHERE brand_name='${brandName}' and product_name='${productName}'`;
    } else if (brandName != null) {
      statement = `SELECT * FROM products WHERE brand_name='${brandName}'`;
    } else if (productName != null) {
      statement = `SELECT * FROM products WHERE product_name='${productName}'`;
    } else
      statement = `SELECT * FROM products WHERE type='${type}'`;

    const connection = db.openConnection();
    connection.query(statement, (error, data) => {
      connection.end();
      response.send(utils.createRequest(error, data));
    });
  } else {
    response.send(utils.createRequest("something went wrong", null));
  }
});

//--------------------------------------
//      get details of one product
//--------------------------------------

router.get("/getProductDetails/:id", (request, response) => {
  const { id } = request.params;
  if (request.session.user) {
    const statement = `SELECT * FROM products WHERE id = ?`;

    const connection = db.openConnection();

    connection.query(statement, [id], (error, result) => {
      connection.end();
      response.send(utils.createRequest(error, result));
    });
  } else {
    response.send(utils.createRequest("something went wrong"));
  }
});

//-------------------------------------------
//              buy one product
//-------------------------------------------

router.post("/buy", (request, response) => {
  const { vendor_id, product_id, product_name, amount, quantity, unit } = request.body;
  if (request.session.user) {
    const statement = `INSERT INTO ongoing_delivery (customer_id,vendor_id,product_id,product_name,amount,
        quantity,unit) VALUES (?,?,?,?,?,?,?);` ;

    const connection = db.openConnection();

    connection.query(statement, [request.session.user.id, vendor_id, product_id, product_name, amount,
      quantity, unit], (error, result) => {
        connection.end();
        response.send(utils.createRequest(error, result));
      });
  } else {
    response.send(utils.createRequest("something went wrong"));
  }
});

router.post("/buyAll", (request, response) => {
  if (request.session.user) {
    var customerId = request.session.user.id
    const sel = `SELECT customer_id,vendor_id,product_id,product_name,amount,
      quantity,unit FROM add_to_cart WHERE customer_id=${customerId}`;
    const connection = db.openConnection();
    let r;

    connection.query(sel, (error, result) => {
      if (result) {
        r = result.map(a => {
          return Object.values(a);
        })
        callme(r)
        console.log(r)
      }
      else {
        connection.end();
        response.send(utils.createRequest(error, result));
      }
    })

    const callme = (abc) => {
      console.log(abc)
      const ins = `INSERT INTO ongoing_delivery (customer_id,vendor_id,product_id,product_name,amount,quantity,unit) VALUES ?`;
      connection.query(
        ins, [abc], (error, result) => {
          if (result) {
            deleteme()
          } else {
            connection.end();
            response.send(utils.createRequest(error, result));
          }
        }
      );
    }
    const deleteme = () => {
      const statement = `delete from add_to_cart where customer_id=${customerId}`;
      connection.query(statement, (error, result) => {
        connection.end();
        response.send(utils.createRequest(error, result));
      });
    }
  } else {
    response.send(utils.createRequest("something went wrong"));
  }
});




module.exports = router;
