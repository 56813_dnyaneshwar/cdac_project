import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../config'
import { Link } from 'react-router-dom'

const DeliveryNavbar = () => {
    const navigate = useNavigate()
    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand nav-link" to="/DeliveryHome">
                <strong  >HIRT | Delivery</strong>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">

                    <li className="nav-item">
                        <Link className="nav-link" aria-current="page" to={'/PendingDelivery'} >Pending Orders</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" aria-current="page" to={'/DeliveryProfile'} >My Profile</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="" className="nav-link" aria-current="page" onClick={Logout} >Log Out</Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default DeliveryNavbar