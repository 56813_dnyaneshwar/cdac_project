import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../config'
import { Link } from 'react-router-dom'

const VendorNavbar = () => {
    const navigate = useNavigate()
    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <Link className="navbar-brand nav-link" to="/VendorHome">
                <strong  >HIRT | Vendor</strong>
            </Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon" />
            </button>
            <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav">

                    <li className="nav-item">
                        <Link className="nav-link" aria-current="page" to="/AddProduct">Add Product</Link>
                    </li>
                    <li className="nav-item">
                        <Link className="nav-link" aria-current="page" to="/VendorProfile">Profile</Link>
                    </li>

                    <li className="nav-item">
                        <Link to="" className="nav-link" aria-current="page" onClick={Logout} >Log Out</Link>
                    </li>
                </ul>
            </div>
        </nav>
    )
}

export default VendorNavbar