import './Product.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { toast } from 'react-toastify';
import axios from 'axios';
import { useNavigate } from 'react-router';
import { URL } from '../config'

const Product = (props) =>{
    const { product } = props
    const productId = product.id
    const navigate = useNavigate()
    
    axios.defaults.withCredentials = true;
    
    const updateProduct = ()=>{
        sessionStorage['productId'] = productId
        navigate('/UpdateProduct')
    }

    const deleteProduct = () =>{
        const body = {
                        productId
                     }
        const url = `${URL}/product/delete`
        axios.post(url,body).then((response)=>{
            const result = response.data
            if(result['status'] === 'success'){
                window.location.reload(false);
            } else{
                toast.error('Failed to Delete')
            }
        })
    }

    return (
        <div className="product">
            <div className ="product-card">
                <div className="product-info">
                    <h5>{product.product_name} ({product.brand_name})</h5>
                    <h6>Stock : {product.stock}</h6>
                    <h6>Max. Shelf Life : {product.max_shelf_life}</h6>
                    <h6>Price : {product.price} Rs / {product.unit}</h6>
                    <div className="form">
                        <button  className='btn btn-dark' 
                        onClick={updateProduct}
                        >Update</button>
                        <button className='btn btn-danger' 
                        onClick={deleteProduct}>Delete</button>
                    </div>

                </div>
            </div>
        </div>
    )
}

export default Product  