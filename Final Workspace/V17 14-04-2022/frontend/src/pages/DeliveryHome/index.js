import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import DeliveryOrder from '../../components/DeliveryOrder';
import DeliveryNavbar from '../../components/DeliveryNavbar';

const DeliveryHome = () => {
    const [deliveryId, setDeliveryId] = useState('')
    const [orders, setOrders] = useState([])



    axios.defaults.withCredentials = true;

    const getDeliveryHome = () => {
        const url = `${URL}/delivery/get`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setOrders(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/delivery/getDeliveryId`
        axios.get(url).then((response) => {
            const result = response.data
            setDeliveryId(result['data'].deliveryId)
        })
        getDeliveryHome()
    }, [])

    return (
        <div>
            {deliveryId &&
                <div>
                    <DeliveryNavbar />
                    <div>
                        {orders.map((order) => {
                            return <DeliveryOrder order={order} />
                        })}
                    </div>
                </div> || <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddDeliveryMan'} >Add Delivery Man Details</Link>
                </div>
            }
        </div>
    )
}

export default DeliveryHome