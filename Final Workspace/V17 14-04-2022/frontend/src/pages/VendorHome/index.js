import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import Product from '../../components/Product';
import VendorNavbar from '../../components/VendorNavbar';

const VendorHome = () => {
    const [vendorId, setVendorId] = useState('')
    const [products, setProducts] = useState([])

    axios.defaults.withCredentials = true;

    const getVendorHome = () => {
        const url = `${URL}/product/getAll`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setProducts(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/vendor/getVendorId`
        axios.get(url).then((response) => {
            const result = response.data
            setVendorId(result['data'].vendorId)
        })
        getVendorHome()
    }, [])

    return (
        <div>
            {vendorId &&
                <div>
                    <VendorNavbar />
                    <div>
                        {products.map((product) => {
                            return <Product product={product} />
                        })}
                    </div>
                </div> || <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddBusiness'} >Add Business Details</Link>
                </div>
            }
        </div>
    )
}

export default VendorHome