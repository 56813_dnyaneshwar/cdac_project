import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import { useNavigate } from 'react-router';
import AdminNavbar from '../../components/AdminNavbar';

const AdminHome = () => {

    const [adminId, setAdminId] = useState('')
    const [roleCount, setRoleCount] = useState([])

    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const getAdminHome = () => {
        const url = `${URL}/admin/count`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setRoleCount(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/admin/getAdminId`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setAdminId(result['data'].adminId)
            } else {
                toast.error('Failed to LogIn')
            }
        })
        getAdminHome()
    }, [])

    return (
        <div>
            {adminId && roleCount &&
                <div>
                    <AdminNavbar />
                    <div>
                        {
                            roleCount.map((c) => {
                                return (
                                    <div className='button5'>
                                        <Link className='button5' to={`/${c.role}List`} >{c.role} <br /> {c.count}</Link>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            }
        </div>
    )

}

export default AdminHome