import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import { useNavigate } from 'react-router';
import UserComponent from '../../components/UserComponent';
import AdminNavbar from '../../components/AdminNavbar';

const CustomerList = () => {

    const [adminId, setAdminId] = useState('')
    const [customerList, setCustomerList] = useState([])
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const getCustomerList = () => {
        const url = `${URL}/admin/getCustomer`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setCustomerList(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/admin/getAdminId`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setAdminId(result['data'].adminId)
            } else {
                toast.error('Failed to LogIn')
            }
        })
        getCustomerList()
    }, [])

    return (
        <div>
            {adminId &&
                <div>
                    <AdminNavbar />
                    <div>
                        {
                            customerList.map((u) => {
                                return <UserComponent u={u}></UserComponent>
                            })
                        }

                    </div>
                </div>
            }
        </div>
    )
}

export default CustomerList

// {c.first_name} {c.last_name} {c.email} {c.phone_no} {c.is_active}