import './index.css'
import Product from '../../components/CustomerProducts'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const CustomerHome = () => {
  const [brandName, setBrandName] = useState([])
  const [productName, setProductName] = useState([])
  const [type, setType] = useState([])
  const [a, setA] = useState('')
  const navigate = useNavigate()

  axios.defaults.withCredentials = true;

  const brand = () => {
    const body = {
      brandName: a ? a : 'Milk',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setBrandName(result['data'])
    })
  }

  const product = () => {
    const body = {
      productName: a ? a : 'Milk-Bread',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setProductName(result['data'])
    })
  }

  const getType = () => {
    const body = {
      type: a ? a : 'Fruit',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setType(result['data'])
    })
  }


  const caller1 = () => {
    product()
    brand()
    getType()
  }

  useEffect(() => {
    caller1()
    console.log('getting called use effect')
  }, [])

  const Logout = () => {
    const url = `${URL}/logout`
    axios.post(url).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        navigate('/')
        toast.success(result['data'])
      } else {
        toast.error('Failed to Logout')
      }
    })
  }

  return (
    <div>
      <CustomerNavbar />
      <div id="CustomerHome" >
        <div className="form">
          <div className="mb-3">
            <input id="email" type="email" className="form-control" placeholder="Search Product ..." onChange={(e) => { setA(e.target.value) }} />
            <div className="text-center">
              <button id='button' className="btn btn-light" onClick={caller1}>Search</button>
            </div>
          </div>
        </div>
      </div>

      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {brandName && brandName.map((product) => {
            return <Product product={product} />
          })}
        </div>
      </div>
      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {productName && productName.map((product) => {
            return <Product product={product} />
          })}
        </div>
      </div>
      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {type && type.map((product) => {
            return <Product product={product} />
          })}
        </div>
      </div>
    </div>
  )
}

export default CustomerHome