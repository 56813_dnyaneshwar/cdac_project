import './index.css'
import Product from '../../components/CustomerProducts'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const CustomerAllProducts = () => {
  const [allProducts, setAllProducts] = useState([])

  axios.defaults.withCredentials = true;

  useEffect(() => {
    const url = `${URL}/customer/getAllProducts`
    axios.get(url).then((response) => {
      const result = response.data
      setAllProducts(result['data'])
    })
    console.log('getting called use effect')
  }, [])

  return (
    <div>
      <CustomerNavbar />
      <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
        <div className="col">
          {allProducts && allProducts.map((product) => {
            return <Product product={product} />
          })}
        </div>
      </div>
    </div>
  )
}

export default CustomerAllProducts