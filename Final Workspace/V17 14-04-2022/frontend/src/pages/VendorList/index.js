import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css'
import { useEffect, useState } from "react"
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import UserComponent from '../../components/UserComponent';
import AdminNavbar from '../../components/AdminNavbar';

const VendorList = () => {

    const [adminId, setAdminId] = useState('')
    const [vendorList, setVendorList] = useState([])

    axios.defaults.withCredentials = true;

    const getCustomerList = () => {
        const url = `${URL}/admin/getVendor`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setVendorList(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/admin/getAdminId`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setAdminId(result['data'].adminId)
            } else {
                toast.error('Failed to LogIn')
            }
        })
        getCustomerList()
    }, [])

    return (
        <div>
            {adminId &&
                <div>
                    <AdminNavbar />
                    <div>
                        {
                            vendorList.map((u) => {
                                return <UserComponent u={u}></UserComponent>
                            })
                        }
                    </div>
                </div>
            }
        </div>
    )
}

export default VendorList