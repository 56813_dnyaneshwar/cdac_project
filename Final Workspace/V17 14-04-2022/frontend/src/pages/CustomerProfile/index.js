import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import axios from 'axios'
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'
import { Link } from 'react-router-dom'

const CustomerProfile = () => {
    const [user, setUser] = useState([])


    axios.defaults.withCredentials = true;
    useEffect(() => {
        const url = `${URL}/user/getCustomerProfile`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUser(result['data'])
        })
    }, [])

    return (
        <div>
            <CustomerNavbar />
            <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                <div className="container">
                    <div className="row justify-content-center" style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.9)', fontSize: '30px', opacity: '0.9' }}>
                        <div className="col-xl-5 col-md-8">
                            <div className="bg-white  rounded-5 shadow-5-strong p-5">
                                <div className="form-outline mb-4">
                                    <h3><label className="label-control">Profile Details</label></h3>
                                    <label className="label-control">First Name : {user.first_name}</label><br />
                                    <label className="label-control">Last Name : {user.last_name}</label><br />
                                    <label className="label-control">Email Id : {user.email}</label><br />
                                    <label className="label-control">Phone Number : {user.phone_no}</label><br />
                                    <label className="label-control">Role : {user.role}</label><br />
                                    <Link to={'/ChangePassword'} >Change Password</Link>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CustomerProfile