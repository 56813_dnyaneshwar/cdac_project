const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//------------------------------
//      /vendor/getVendorProfile
//------------------------------
router.get('/getVendorProfile',(request,response)=>{
    if(request.session.user){
        const statement = `select u.first_name,u.last_name,u.email,u.phone_no,u.role,v.owner_name,v.shop_name,v.gst_no from users u inner join vendors v on u.id=v.user_id where v.id=?`
        const connection = db.openConnection()
        if(request.session.vendorId){
            connection.query(statement,[request.session.vendorId],(error,result)=>{
                connection.end()
                if(result.length > 0)
                response.send(utils.createRequest(error,result[0]))
                else{
                    error1 = "Vendor Does Not Exist"
                    response.send(utils.createRequest(error1))
                }
            })
        }
    } else {
        const error = "User Not Found"
        response.send(utils.createRequest(error))
    } 
})

//-----------------------------------
//      /vendor/getVendorId
//-----------------------------------
router.get('/getVendorId',(request,response)=>{
    if(request.session.vendorId){
        response.send(utils.createRequest(null,{ vendorId : request.session.vendorId}))
    } else {
        if(request.session.user){
        const userId = request.session.user.id
        const statement = `SELECT id FROM vendors WHERE user_id = ?`
        const connection = db.openConnection()
            connection.query(statement,[userId],(error,result)=>{
                connection.end()
                if(result.length > 0){
                    request.session.vendorId = result[0].id                 //adding vendorId in session
                    response.send(utils.createRequest(error,{ vendorId : result[0].id}))
                }
            })
        } else {
                response.send(utils.createRequest("User Not Found"))
        }
    }
})

//-----------------------------------
//      /vendor/add
//-----------------------------------
router.post('/add',(request,response)=>{
    if(request.session.user){
        const userId = request.session.user.id
        const role = request.session.user.role
    
        const { ownerName, shopName, gstNo } = request.body
    
        const statement = `INSERT INTO vendors (user_id, owner_name, shop_name, gst_no) VALUES (?,?,?,?)`
    
        if(role === 'vendor'){
            const connection = db.openConnection()
            connection.query(statement,[userId, ownerName, shopName, gstNo],(error,result)=>{
                connection.end()
                response.send(utils.createRequest(error,result))
            })
        }
        else{
            const error = "User is not a Vendor"
            response.send(utils.createRequest(error))
        }
    } else {
        error = 'User Not Found'
        response.send(utils.createRequest(error))
    }
})

//-----------------------------------
//      /vendor/update -> by vendorId for Admin
//-----------------------------------
router.put('/update',(request,response)=>{
    const { vendorId } = request.body
    const statement = `UPDATE vendors SET owner_name = ?, shop_name = ?, gst_no = ? WHERE id = ?`
    const connection = db.openConnection()
    connection.query(statement,[ownerName, shopName, gstNo, vendorId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

//-----------------------------------
//      /vendor/delete -> by vendorId for Admin
//-----------------------------------
router.delete('/delete',(request,response)=>{
    const { vendorId } = request.body
    const statement = `DELETE FROM vendors WHERE id = ?`
    const connection = db.openConnection()
    connection.query(statement,[vendorId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

module.exports = router

        // Add Vendor
// { 
//     "ownerName" : "Hitendra Dhande",
//     "shopName" : "Shivshakti Mart",
//     "gstNo" :  "GSTIN5132465"
// }

        // Add Product
// {
//     "category" : "Fresh-Food", 
//     "type" : "Fruit", 
//     "paymentMethod" : 1,
//     "brandName" : "Kashmir",
//     "productName" : "Apple",
//     "maxShelfLife" : 3,
//     "moreDetails" : "An apple is an edible fruit produced by an apple tree. Apple trees are cultivated worldwide and are the most widely grown species in the genus Malus. The tree originated in Central Asia, where its wild ancestor, Malus sieversii, is still found today.",
//     "price" : "45",
//     "unit" : "kg",
//     "stock" :  100
// }