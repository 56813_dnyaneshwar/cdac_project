const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//------------------------------
//      /account/add
//------------------------------
router.post('/add',(request,response)=>{
    const userId = request.session.user.id
    const { bankName, accountHolderName, accountNo, ifscCode } = request.body
    const statement = `
    INSERT INTO
    accounts (user_id, bank_name, account_holder_name, account_no, ifsc_code)
    VALUES (?, ?, ?, ?, ?)`
    if(userId !== null){
        const connection = db.openConnection()
        connection.query(statement,[userId, bankName, accountHolderName, accountNo, ifscCode],(error,result)=>{
            connection.end()
            response.send(utils.createRequest(error,result))
        })
    } else {
        const error = "User Not Found"
        response.send(utils.createRequest(error))
    }
})

//------------------------------
//      /account/getById
//------------------------------
router.get('/getById',(request,response)=>{
    const userId = request.session.user.id
    const statement = `SELECT * FROM accounts WHERE user_id = ?`
    const connection = db.openConnection()
    connection.query(statement,[userId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})

//------------------------------
//      /account/update
//------------------------------
router.post('/update',(request,response)=>{
    const userId = request.session.user.id
    const { bankName, accountHolderName, accountNo, ifscCode } = request.body
    const statement = `
    UPDATE accounts SET bank_name = ?, account_holder_name = ?, account_no = ?, ifsc_code = ? WHERE user_id = ?`
    const connection = db.openConnection()
    connection.query(statement,[bankName, accountHolderName, accountNo, ifscCode, userId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})

//------------------------------
//      /account/delete
//------------------------------
router.post('/delete',(request,response)=>{
    const userId = request.session.user.id
    const statement = `DELETE FROM accounts WHERE user_id = ?`
    const connection = db.openConnection()
    connection.query(statement,[userId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})

module.exports = router