const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.get('/getVendor',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role=vendor`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result>0){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

router.get('/getCustomer',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role=customer`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result>0){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

router.get('/getDelivery',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role=delivery`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result>0){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

router.delete('/delete',(request,response)=>{
    const { id } = request.body
    const statement =  `DELETE FROM users WHERE id=?`
    const connection=db.openConnection()
    connection.query(statement,[id],(error,result)=>{
        connection.end()
        if(result>0){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

router.delete('/count',(request,response)=>{
    const { id } = request.body
    const statement =  `SELECT role,COUNT(*) as count FROM users WHERE role!='admin' GROUP BY role ORDER BY role ASC`
    const connection=db.openConnection()
    connection.query(statement,[id],(error,result)=>{
        connection.end()
        if(result>0){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

module.exports = router