CREATE DATABASE Project_HIRT_Express;
USE Project_HIRT_Express;

CREATE TABLE users(
id INT PRIMARY KEY AUTO_INCREMENT,
first_name VARCHAR(30),
last_name VARCHAR(30),
email VARCHAR(50) UNIQUE,
password VARCHAR(600),
phone_no BIGINT,
role VARCHAR(30),
is_active BOOLEAN
);

CREATE TABLE vendors(
id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT UNIQUE NOT NULL,
owner_name VARCHAR(30),
shop_name VARCHAR(100),
gst_no VARCHAR(50),
FOREIGN KEY(user_id) REFERENCES users(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE addresses(
id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT,
address_line1 VARCHAR(100),
address_line2 VARCHAR(100),
city VARCHAR(50),
postal_code INT,
state VARCHAR(50),
country VARCHAR(50),
FOREIGN KEY(user_id) REFERENCES users(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE accounts(
id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT,
bank_name VARCHAR(50),
account_holder_name VARCHAR(50),
account_no VARCHAR(50),
ifsc_code VARCHAR(50),
FOREIGN KEY(user_id) REFERENCES users(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE payments(
id INT PRIMARY KEY AUTO_INCREMENT,
user_id INT,
transaction_id VARCHAR(50),
credit_amount DOUBLE DEFAULT 0,
debit_amount DOUBLE DEFAULT 0,
created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY(user_id) REFERENCES users(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE products(
id INT PRIMARY KEY AUTO_INCREMENT,
vendor_id INT,
category VARCHAR(50),
type VARCHAR(50),
payment_method INT(1),
brand_name VARCHAR(50),
product_name VARCHAR(50),
max_shelf_life INT,
more_details VARCHAR(300),
price DOUBLE,
unit VARCHAR(10),
stock INT,
created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY(vendor_id) REFERENCES vendors(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);

CREATE TABLE order_history(
id INT PRIMARY KEY AUTO_INCREMENT,
order_id INT,
customer_id INT,
vendor_id INT,
product_id INT,
product_name VARCHAR(100),
amount DOUBLE,
quantity INT,
unit VARCHAR(10),
status VARCHAR(10),
created_timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
FOREIGN KEY(customer_id) REFERENCES users(id)
ON DELETE CASCADE
ON UPDATE CASCADE,
FOREIGN KEY(vendor_id) REFERENCES vendors(id)
ON DELETE CASCADE
ON UPDATE CASCADE,
FOREIGN KEY(product_id) REFERENCES products(id)
ON DELETE CASCADE
ON UPDATE CASCADE
);



------------------------->vendor Home page view
CREATE VIEW vendorHome AS
SELECT 
o.order_id,
o.vendor_id,
CONCAT(c.first_name,c.last_name) AS customer_name,
o.product_name,
o.quantity,
o.unit,
o.status
FROM order_history o
INNER JOIN registration c 
ON 
c.id=o.customer_id;