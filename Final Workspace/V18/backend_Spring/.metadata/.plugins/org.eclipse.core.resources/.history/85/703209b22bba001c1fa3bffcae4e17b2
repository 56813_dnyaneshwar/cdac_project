package hirt.project.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import hirt.project.entities.Product;
import hirt.project.services.ProductService;

@CrossOrigin(origins = "http://localhost:3000", maxAge = 3600)
@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@GetMapping("/getById/{id}")
	public ResponseEntity<?> getById(@PathVariable int id) {
		return ResponseEntity.ok(productService.getProductById(id));
	}

	@GetMapping("/getAll/{vendorId}")
	public ResponseEntity<?> getByVendorId(@PathVariable int vendorId) {
		return ResponseEntity.ok(productService.getProductByVendorId(vendorId));
	}

	@PostMapping("/add")
	public ResponseEntity<?> addProduct(@RequestBody Product product) {
		Product p = productService.addProduct(product);
		if (p == null) {
			return ResponseEntity.badRequest().body(null);
		}
		return new ResponseEntity<>(p, HttpStatus.CREATED);
	}

	@PostMapping("/update")
	public ResponseEntity<?> updateProduct(@RequestBody Product product) {
		Product p = productService.getProductById(product.getId());
		if (p == null) {
			return ResponseEntity.badRequest().body(null);
		}
		p.setPaymentMethod(product.getPaymentMethod());
		p.setMaxShelfLife(product.getMaxShelfLife());
		p.setMoreDetails(product.getMoreDetails());
		p.setPrice(product.getPrice());
		p.setUnit(product.getUnit());
		p.setStock(product.getStock());
		p = productService.updateProduct(p);
		return new ResponseEntity<>(p, HttpStatus.CREATED);
	}

}
