package hirt.project.daos;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import hirt.project.entities.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {
	Product findById(int id);

	List<Product> findByVendorId(int vendorId);
}
