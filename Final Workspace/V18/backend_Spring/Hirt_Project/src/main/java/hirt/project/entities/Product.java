package hirt.project.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "products")
public class Product {

	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Id
	private int id;
	private int vendorId;
	private String category;
	private String type;
	private int paymentMethod;
	private String brandName;
	private String productName;
	private int maxShelfLife;
	private String moreDetails;
	private double price;
	private String unit;
	private int stock;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(insertable = false)
	private Date createdTimestamp;

	public Product() {

	}

	public Product(int id, int vendorId, String category, String type, int paymentMethod, String brandName,
			String productName, int maxShelfLife, String moreDetails, double price, String unit, int stock,
			Date createdTimestamp) {
		this.id = id;
		this.vendorId = vendorId;
		this.category = category;
		this.type = type;
		this.paymentMethod = paymentMethod;
		this.brandName = brandName;
		this.productName = productName;
		this.maxShelfLife = maxShelfLife;
		this.moreDetails = moreDetails;
		this.price = price;
		this.unit = unit;
		this.stock = stock;
		this.createdTimestamp = createdTimestamp;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getVendorId() {
		return vendorId;
	}

	public void setVendorId(int vendorId) {
		this.vendorId = vendorId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(int paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getBrandName() {
		return brandName;
	}

	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getMaxShelfLife() {
		return maxShelfLife;
	}

	public void setMaxShelfLife(int maxShelfLife) {
		this.maxShelfLife = maxShelfLife;
	}

	public String getMoreDetails() {
		return moreDetails;
	}

	public void setMoreDetails(String moreDetails) {
		this.moreDetails = moreDetails;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getStock() {
		return stock;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	public Date getCreatedTimestamp() {
		return createdTimestamp;
	}

	public void setCreatedTimestamp(Date createdTimestamp) {
		this.createdTimestamp = createdTimestamp;
	}

	@Override
	public String toString() {
		return String.format(
				"Product [id=%s, vendorId=%s, category=%s, type=%s, paymentMethod=%s, brandName=%s, productName=%s, maxShelfLife=%s, moreDetails=%s, price=%s, unit=%s, stock=%s, createdTimestamp=%s]",
				id, vendorId, category, type, paymentMethod, brandName, productName, maxShelfLife, moreDetails, price,
				unit, stock, createdTimestamp);
	}

}
