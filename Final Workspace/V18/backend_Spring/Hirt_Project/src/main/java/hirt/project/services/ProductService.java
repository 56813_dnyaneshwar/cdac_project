package hirt.project.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import hirt.project.daos.ProductDao;
import hirt.project.entities.Product;

@Service
public class ProductService {

	@Autowired
	private ProductDao productDao;

	public Product getProductById(int id) {
		return productDao.findById(id);
	}

	public List<Product>getProductByVendorId(int vendorId) {
		return productDao.findByVendorId(vendorId);
	}

	public Product addProduct(Product product) {
		return productDao.save(product);
	}
	
	public Product updateProduct(Product product) {
		return productDao.save(product);
	}
}
