import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import VendorNavbar from '../../components/VendorNavbar';

const AddProduct = () => {
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')

    const [category, setCategory] = useState('')
    const [type, setType] = useState('')
    const [paymentMethod, setPaymentMethod] = useState(1)
    const [brandName, setBrandName] = useState('')
    const [productName, setProductName] = useState('')
    const [maxShelfLife, setMaxShelfLife] = useState('')
    const [moreDetails, setMoreDetails] = useState('')
    const [price, setPrice] = useState('')
    const [unit, setUnit] = useState('')
    const [stock, setStock] = useState('')

    const navigate = useNavigate()


    axios.defaults.withCredentials = true;

    const addProductDetails = () => {
        if (userId !== null) {
            const body = {
                category, type, paymentMethod, brandName,
                productName, maxShelfLife, moreDetails, price, unit, stock
            }
            const url = `${URL}/product/add`
            axios.post(url, body).then((response) => {
                const result = response.data
                if (result['status'] == 'success') {
                    toast.success('Product Added Successfully')
                    navigate('/VendorHome')
                } else {
                    toast.error('Invalid')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(() => {
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    }, [])

    const clearInput = () => {
        const CategoryInput = document.getElementById('Category')
        CategoryInput.value = ""
        const TypeInput = document.getElementById('Type')
        TypeInput.value = ""
        const PaymentMethodInput = document.getElementById('PaymentMethod')
        PaymentMethodInput.value = ""
        const BrandNameInput = document.getElementById('BrandName')
        BrandNameInput.value = ""
        const ProductNameInput = document.getElementById('ProductName')
        ProductNameInput.value = ""
        const MaxShelfLifeInput = document.getElementById('MaxShelfLife')
        MaxShelfLifeInput.value = ""
        const MoreDetailsInput = document.getElementById('MoreDetails')
        MoreDetailsInput.value = ""
        const PriceInput = document.getElementById('Price')
        PriceInput.value = ""
        const UnitInput = document.getElementById('Unit')
        UnitInput.value = ""
        const StockInput = document.getElementById('Stock')
        StockInput.value = ""
    }

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <div>
            {userId && role === 'vendor' &&

                <div>
                    {/*Main Navigation*/}
                    <header>
                        <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image1.jpg);background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                        <VendorNavbar />
                        {/* Background image */}
                        <div id="intro" className="bg-image shadow-2-strong">
                            <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0.8)' }}>
                                <div className="container">
                                    <div className="row justify-content-center" style={{ color: 'rgba(255, 255, 255, 255)' }}>
                                        <h1 className='title' style={{ color: '#202020' }} >Add Product</h1>
                                        <div className="col-xl-5 col-md-8">
                                            <div className="mb-3">
                                                <label className="label-control">Category</label>
                                                <input id="Category" type="text" className="form-control" placeholder="Fresh-Food / Packaged-Food"
                                                    onChange={(e) => {
                                                        setCategory(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Type</label>
                                                <input id="Type" type="text" className="form-control" placeholder="Fruit, Vegitable, Dairy, etc."
                                                    onChange={(e) => {
                                                        setType(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <label className="label-control">PaymentMethod</label>
                                            <div style={{ textAlign: 'center' }}>
                                                <label className='form-check-label'>COD : </label>
                                                <input className='form-check-input'
                                                    type="radio"
                                                    checked={paymentMethod === "0"}
                                                    value="0"
                                                    onChange={(e) => setPaymentMethod(e.target.value)} />
                                            </div>
                                            <div style={{ textAlign: 'center' }}>
                                                <label className='form-check-label'>Online : </label>
                                                <input className='form-check-input'
                                                    type="radio"
                                                    checked={paymentMethod === "1"}
                                                    value="1"
                                                    onChange={(e) => setPaymentMethod(e.target.value)} />
                                            </div>
                                            <div className="mb-3">
                                                <label className="label-control">Brand Name</label>
                                                <input id="BrandName" type="text" className="form-control" placeholder="lorem Ipsum's"
                                                    onChange={(e) => {
                                                        setBrandName(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Product Name</label>
                                                <input id="ProductName" type="text" className="form-control" placeholder="eg. Apple, Banana, Dairy Milk, etc."
                                                    onChange={(e) => {
                                                        setProductName(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Maximum Shelf Life</label>
                                                <input id="MaxShelfLife" type="number" className="form-control" placeholder="1,2,3,4..."
                                                    onChange={(e) => {
                                                        setMaxShelfLife(e.target.value)
                                                    }}
                                                />
                                            </div>
                                            <div className="mb-3">
                                                <label className="label-control">More Details</label>
                                                <input id="MoreDetails" type="text" className="form-control" placeholder="This is ..."
                                                    onChange={(e) => {
                                                        setMoreDetails(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Price</label>
                                                <input id="Price" type="number" className="form-control" placeholder="eg.50rs, 120rs, ..."
                                                    onChange={(e) => {
                                                        setPrice(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Unit</label>
                                                <input id="Unit" type="text" className="form-control" placeholder="eg. kg, g, l, ml, piece, etc"
                                                    onChange={(e) => {
                                                        setUnit(e.target.value)
                                                    }}
                                                />
                                            </div>
                                            <div className="mb-3">
                                                <label className="label-control">Stock</label>
                                                <input id="Stock" type="number" className="form-control" placeholder="50, 100, 150, ..."
                                                    onChange={(e) => {
                                                        setStock(e.target.value)
                                                    }}
                                                />
                                            </div>
                                            <div className="button">
                                                <button className="btn btn-primary"
                                                    onClick={addProductDetails}
                                                >Add Product</button>
                                            </div>
                                            <div className="button">
                                                <button className="btn btn-danger"
                                                    onClick={clearInput}
                                                >Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Background image */}
                    </header>
                    {/*Main Navigation*/}
                </div>
                || <div style={{ color: 'red', textAlign: 'center' }}>
                    <h1>Sorry You Are Not Vendor !!!</h1>
                </div>


            }
        </div>
    )
}

export default AddProduct