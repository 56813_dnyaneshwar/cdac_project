import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from "react"
import { Link } from 'react-router-dom'
import { toast } from "react-toastify"
import axios from "axios"
import { URL } from '../../config'
import Product from '../../components/Product';
import { useNavigate } from 'react-router';
import VendorNavbar from '../../components/VendorNavbar';
import Footer from '../../components/Footer'
const VendorHome = () => {
    const [vendorId, setVendorId] = useState('')
    const [products, setProducts] = useState([])
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const getVendorHome = () => {
        const url = `${URL}/product/getAll`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setProducts(result['data'])
            } else {
                toast.error('Login Again / Reload the Page')
            }
        })
    }

    useEffect(() => {
        const url = `${URL}/vendor/getVendorId`
        axios.get(url).then((response) => {
            const result = response.data
            setVendorId(result['data'].vendorId)
        })
        getVendorHome()
    }, [])

    return (
        <div>
            {vendorId &&
                <div>
                    <header>
                        <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image1.jpg);height: 100vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                        <VendorNavbar />
                        {/* Background image */}
                        <div id="intro" className="bg-image shadow-2-strong">
                            <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                                <div className="container">
                                    <div className="row justify-content-center">
                                        <div>
                                            {products.map((product) => {
                                                return <Product product={product} />
                                            })}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {/* Background image */}
                    </header>
                    <Footer />
                </div> || <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddBusiness'} >Add Business Details</Link>
                </div>
            }
        </div>
    )
}

export default VendorHome