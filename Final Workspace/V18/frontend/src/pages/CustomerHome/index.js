import './index.css'
import Product from '../../components/CustomerProducts'
import { useState, useEffect } from 'react'
import axios from 'axios'
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const CustomerHome = () => {
  const [brandName, setBrandName] = useState([])
  const [productName, setProductName] = useState([])
  const [type, setType] = useState([])
  const [a, setA] = useState('')

  axios.defaults.withCredentials = true;

  const brand = () => {
    const body = {
      brandName: a ? a : 'Milk',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setBrandName(result['data'])
    })
  }

  const product = () => {
    const body = {
      productName: a ? a : 'Milk-Bread',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setProductName(result['data'])
    })
  }

  const getType = () => {
    const body = {
      type: a ? a : 'Fruit',
    }
    const url = `${URL}/customer/search/byCategory`
    axios.post(url, body).then((response) => {
      const result = response.data
      setType(result['data'])
    })
  }


  const caller1 = () => {
    product()
    brand()
    getType()
  }

  useEffect(() => {
    caller1()
    console.log('getting called use effect')
  }, [])

  return (
    <div>

      <div>
        {/*Main Navigation*/}
        <header>
          <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image15.jpg);height: 100vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
            <CustomerNavbar />
          {/* Background image */}
          <div id="intro" className="bg-image shadow-2-strong">
            <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
              <div className="container">
                <div className="row justify-content-center">
                  <div id="CustomerHome" >
                    <div className="form">
                      <div className="mb-3">
                        <input id="email" type="email" className="form-control" placeholder="Search Product ..." onChange={(e) => { setA(e.target.value) }} />
                        <div className="text-center">
                          <button id='button' className="btn btn-light" onClick={caller1}>Search</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
                    <div className="col">
                      {brandName && brandName.map((product) => {
                        return <Product product={product} />
                      })}
                    </div>
                  </div>
                  <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
                    <div className="col">
                      {productName && productName.map((product) => {
                        return <Product product={product} />
                      })}
                    </div>
                  </div>
                  <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
                    <div className="col">
                      {type && type.map((product) => {
                        return <Product product={product} />
                      })}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Background image */}
        </header>
        {/*Main Navigation*/}
        {/*Footer*/}
        <footer className="bg-light text-lg-start">
          {/* Copyright */}
          <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
            © 2022 Copyright:
            <a className="text-dark" href="http://localhost:3000/">HIRT_Project_Sunbeam</a>
          </div>
          {/* Copyright */}
        </footer>
        {/*Footer*/}
      </div>
    </div>
  )
}

export default CustomerHome