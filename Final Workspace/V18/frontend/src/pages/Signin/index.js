import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { Link } from 'react-router-dom'
import { URL } from '../../config'

const Signin = () => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [user, setUser] = useState()
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const signinUser = () => {
        const body = {
            email,
            password,
        }
        const url = `${URL}/user/getByEmailPassword`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setUser(result['data'])
                toast.success('welcome')
                if (user.role === "vendor")
                    navigate('/VendorHome')
                else if (user.role === "customer")
                    navigate('/CustomerHome')
                else if (user.role === "delivery")
                    navigate('/DeliveryHome')
                else if (user.role === "admin")
                    navigate('/AdminHome')
            } else {
                toast.error('Invalid Email / Password')
            }
        })
    }

    const clearInput = () => {
        document.getElementById('email').value = ""
        document.getElementById('password').value = ""
    }

    return (
        <div>
            {/*Main Navigation*/}
            <header>
                <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/Image2.jpg);height: 110vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                {/* Background image */}
                <div id="intro" className="bg-image shadow-2-strong">
                    <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-xl-5 col-md-8">
                                    <div className="bg-white  rounded-5 shadow-5-strong p-5">
                                        {/* Email input */}
                                        <div className="form-outline mb-4">
                                            <h4 style={{ textAlign: 'center' }}>Sign In</h4>
                                            <label className="form-label">Email address</label>
                                            <input type="email" id="email" className="form-control" placeholder="lorem@test.com"
                                                onChange={(e) => {
                                                    setEmail(e.target.value)
                                                }}
                                            />
                                        </div>
                                        {/* Password input */}
                                        <div className="form-outline mb-4">
                                            <label className="form-label">Password</label>
                                            <input type="password" id="password" className="form-control" placeholder="*********"
                                                onChange={(e) => {
                                                    setPassword(e.target.value)
                                                }}
                                            />
                                        </div>
                                        <div>
                                            Not yet Signed Up ? <Link to="/signup">Sign Up</Link> Here

                                        </div>
                                        <button className="btn btn-primary btn-block"
                                            onClick={signinUser}
                                        >Signin</button>
                                        <button className="btn btn-danger"
                                            onClick={clearInput}
                                        >Clear</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Background image */}
            </header>
            {/*Main Navigation*/}
            {/*Footer*/}
            <footer className="bg-light text-lg-start">
                {/* Copyright */}
                <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                    © 2022 Copyright:
                    <a className="text-dark" href="http://localhost:3000/">HIRT_Project_Sunbeam</a>
                </div>
                {/* Copyright */}
            </footer>
            {/*Footer*/}
        </div>
    )
}

export default Signin