import './index.css'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const ProductDetails = () => {

    const product = JSON.parse(sessionStorage["product"])
    const navigate = useNavigate()
    axios.defaults.withCredentials = true;


    const vendor_id = product.vendor_id
    const product_id = product.id
    const product_name = product.product_name
    const amount = product.price
    const quantity = 1
    const unit = product.unit
    console.log(product_name)
    const cart = () => {
        const body = {
            vendor_id,
            product_id,
            product_name,
            amount,
            quantity,
            unit,
        }
        const url = `${URL}/cart/addToCart`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('product added successfully')
            } else {
                toast.error('something went wrong! please try again')
            }
        })
    }

    const goToBuy = () => {
        sessionStorage["product"] = JSON.stringify(product)
        navigate('/Buy')
    }

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <div>
            {/*Main Navigation*/}
            <header>
                <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image15.jpg);height: 100.2vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                <CustomerNavbar />
                {/* Background image */}
                <div id="intro" className="bg-image shadow-2-strong">
                    <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        <div className="container">
                            <div className="row justify-content-center" style={{ textAlign: 'center', color: 'rgba(0, 0, 0, 0.9)', fontSize: '30px', opacity: '0.9' }}>
                                <div className="col-xl-5 col-md-8">
                                    <div className="bg-white  rounded-5 shadow-5-strong p-5">
                                        <div className="form-outline mb-4">
                                            <h3><label className="label-control">{product.product_name} ({product.brand_name})</label></h3>
                                            <label className="label-control">Stock : {product.stock}</label><br />
                                            <label className="label-control">Max. Shelf Life : {product.max_shelf_life}</label>
                                            <label className="label-control">Price : {product.price} Rs / {product.unit}</label>
                                            <div className="form">
                                                <button id='button' className='btn btn-danger' onClick={goToBuy}>Buy Now</button>
                                                <button id='button' className='btn btn-dark' onClick={cart}>Add to Cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div >
                {/* Background image */}
            </header >
            {/*Main Navigation*/}
        </div >
    )

}
export default ProductDetails