import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import 'bootstrap/dist/css/bootstrap.min.css';
import { GiFruitBowl } from 'react-icons/gi'

const AddBusiness = () => {
    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [ownerName, setOwnerName] = useState('')
    const [shopName, setShopName] = useState('')
    const [gstNo, setGstNo] = useState('')
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const addBusinessDetails = ()=>{
        if(userId !== null){
            const body = { 
                            ownerName,
                            shopName,
                            gstNo
                        }
            const url = `${URL}/vendor/add`
            axios.post(url,body).then((response)=>{
                const result = response.data
                if(result['status'] == 'success'){
                    toast.success('Business Added Successfully')
                    navigate('/AddAddress')
                } else{
                    toast.error('Invalid')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(()=>{
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response)=>{
            const result = response.data
            console.log(result)
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    },[])

    const clearInput = () =>{
        const ownerNameInput = document.getElementById('ownerName')
        ownerNameInput.value = ""
        const shopNameInput = document.getElementById('shopName')
        shopNameInput.value = ""
        const gstNoInput = document.getElementById('gstNo')
        gstNoInput.value = ""
    }

return (
    <div>
        <span className="HIRT">HIRT</span><span className="cart"><GiFruitBowl/></span>
            
       { userId && role === 'vendor' &&
        <div className="row">
            <h1 className='title'>Business Details</h1>
            <div className="col"></div>
            <div className='col'>
            <div className="form">
            <div className="mb-3">
                <label className="label-control">Owner Name</label>
                <input id="ownerName" type="text" className="form-control" placeholder="lorem Ipsum"
                onChange={(e)=>{
                    setOwnerName(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">Shop Name</label>
                <input id="shopName" type="text" className="form-control" placeholder="eg.Lorem Ipsum's Pvt. Ltd."
                onChange={(e)=>{
                    setShopName(e.target.value)
                }}
                />
            </div>

            <div className="mb-3">
                <label className="label-control">GST No.</label>
                <input id="gstNo" type="text" className="form-control" placeholder="eg.GSTIN00215485"
                onChange={(e)=>{
                    setGstNo(e.target.value)
                }}
                />
            </div>    
                <div className="button">
                    <button className="btn btn-primary"
                    onClick={addBusinessDetails}
                    >Add Business</button>
                </div>
                <div className="button">
                    <button className="btn btn-danger"
                    onClick={clearInput}
                    >Clear</button>
                </div>
            </div>
            </div> 
            <div className="col"></div>
            </div>

            ||  <div style={{color:'red', textAlign: 'center'}}>
                    <h1>Sorry You Are Not Vendor !!!</h1>
                    <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                </div>
                
            
        }
    </div>
)
}

export default AddBusiness