import './index.css'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const BuyProduct = () => {
    const product = JSON.parse(sessionStorage["product"])
    axios.defaults.withCredentials = true;
    const navigate = useNavigate()
    const [add, setAdd] = useState([])

    const vendor_id = product.vendor_id
    const product_id = product.id
    const product_name = product.product_name
    const amount = product.price
    const quantity = 1
    const unit = product.unit

    const getAdd = () => {
        const url = `${URL}/address/get`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setAdd(result['data'][0])
                console.log(result['data'][0])
                console.log(add)
            }
            else
                toast.error('could not fetch address! please try again')
        })
    }

    const confirmBuy = () => {

        const body = {
            vendor_id,
            product_id,
            product_name,
            amount,
            quantity,
            unit,
        }
        const url = `${URL}/customer/buy`
        axios.post(url, body).then((response) => {
            console.log('inside buy')
            const result = response.data
            console.log(response.data)
            if (result['status'] === 'success') {
                toast.success('Ordered Successfully')
                navigate('/CustomerHome')
            } else {
                toast.error('Something went wrong! Please try again')
            }
        })
    }

    useEffect(() => {
        getAdd()
        console.log(add)
        console.log('getting called')
    }, [])
    return (
        <div>
            {add &&
                <div>
                    <CustomerNavbar />
                    
                    <div id="ConfirmAddressCard">
                        <div className="card">
                            <div className="card-header">
                                Confirm Details
                            </div>
                            <div className="card-body">
                                <h5 className="card-title">Confirm Address and Proceed to Payment</h5>
                                <p className="card-text">{add.address_line1}, {add.address_line2}, {add.city}, {add.state}, {add.country} - {add.postal_code}</p>
                                <Link to={''} onClick={confirmBuy} className="btn btn-primary">Pay and Purchase</Link>
                            </div>
                        </div>
                    </div>
                </div> ||
                <div className='center-screen'>
                    <Link className='btn btn-info' to={'/AddAddress'} >Add Address Details</Link>
                </div>

            }

        </div>
    )
}
export default BuyProduct

// address_line1: "216"
// address_line2: "Shivshakti Electricals and Electronics"
// city: "Jalgaon"
// country: "India"
// id: 4
// postal_code: 425001
// state: "Maharashtra"
// user_id: 11