import './index.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import axios from 'axios'
import { URL } from '../../config'
import { toast } from "react-toastify"
import { useNavigate } from 'react-router';
import CustomerNavbar from '../../components/CustomerNavbar'

const CustomerProfile = () => {
    const [user, setUser] = useState([])

    const navigate = useNavigate()

    axios.defaults.withCredentials = true;
    useEffect(() => {
        const url = `${URL}/user/getCustomerProfile`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUser(result['data'])
        })
    }, [])

    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <div>
            {/*Main Navigation*/}
            <header>
                <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/Image15.jpg);height: 100.2vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                <CustomerNavbar />
                {/* Background image */}
                <div id="intro" className="bg-image shadow-2-strong">
                    <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        <div className="container">
                            <div className="row justify-content-center" style={{ color: 'rgba(0, 0, 0, 0.9)', fontSize: '30px' }}>
                                <label className="label-control">First Name : {user.first_name}</label>
                                <label className="label-control">Last Name : {user.last_name}</label>
                                <label className="label-control">Email Id : {user.email}</label>
                                <label className="label-control">Phone Number : {user.phone_no}</label>
                                <label className="label-control">Role : {user.role}</label>
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                {/* Background image */}
            </header>
            {/*Main Navigation*/}
        </div>
    )
}

export default CustomerProfile