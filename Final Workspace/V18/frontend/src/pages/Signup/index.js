import { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import './index.css'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'

const Signup = () => {

    const [firstName, setFirstname] = useState('')
    const [lastName, setLastname] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [phoneNo, setPhoneNo] = useState('')
    const [role, setRole] = useState('customer')

    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const signupUser = () => {
        const body = { firstName, lastName, email, password, phoneNo, role }
        const url = `${URL}/user/add`
        axios.post(url, body).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('Signed Up Successfully')
                navigate('/')
            } else {
                toast.error('Failed to SignUp')
            }
        })
    }

    const passwordCheck = () => {
        const checkMessage = document.getElementById('passwordCheck')
        if (password && confirmPassword) {
            if (password === confirmPassword) {
                checkMessage.style.color = "green"
                checkMessage.innerHTML = "Matched"
            }
            else {
                checkMessage.style.color = "red"
                checkMessage.innerHTML = "Not Matched"
            }
        }
    }

    const clearInput = () => {
        document.getElementById('firstName').value = ""
        document.getElementById('lastName').value = ""
        document.getElementById('password').value = ""
        document.getElementById('confirmPassword').value = ""
        document.getElementById('email').value = ""
        document.getElementById('phoneNo').value = ""
        document.getElementById('passwordCheck').innerHTML = ""
    }


    return (
        <div>


            <div>
                {/*Main Navigation*/}
                <header>
                    <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/Image2.jpg);height: 110vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                    {/* Background image */}
                    <div id="intro" className="bg-image shadow-2-strong">
                        <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                            <div className="container">
                                <div className="row justify-content-center">
                                    <div className="col-xl-5 col-md-8">
                                        <div className="bg-white  rounded-5 shadow-5-strong p-5">
                                            <h4 style={{ textAlign: 'center' }}>Sign Up</h4>
                                            <div>
                                                <label>Customer : </label>
                                                <input type="radio"
                                                    checked={role === 'customer'}
                                                    value="customer"
                                                    onChange={(e) => setRole(e.target.value)} />
                                                <br />

                                                <label>Vendor   : </label>
                                                <input type="radio"
                                                    checked={role === 'vendor'}
                                                    value="vendor"
                                                    onChange={(e) => setRole(e.target.value)} />
                                                <br />

                                                <label>Delivery : </label>
                                                <input type="radio"
                                                    checked={role === 'delivery'}
                                                    value="delivery"
                                                    onChange={(e) => setRole(e.target.value)} />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Firstname</label>
                                                <input id="firstName" type="text" className="form-control"
                                                    onChange={(e) => {
                                                        setFirstname(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Lastname</label>
                                                <input id="lastName" type="text" className="form-control"
                                                    onChange={(e) => {
                                                        setLastname(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Email</label>
                                                <input id="email" type="email" className="form-control"
                                                    onChange={(e) => {
                                                        setEmail(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Password</label>
                                                <input id="password" type="text" className="form-control"
                                                    onChange={(e) => {
                                                        setPassword(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div className="mb-3">
                                                <label className="label-control">Confirm Password</label>
                                                <input id="confirmPassword" type="text" className="form-control"
                                                    onChange={(e) => {
                                                        setConfirmPassword(e.target.value)
                                                    }}
                                                    onBlur={passwordCheck()}
                                                />
                                            </div>
                                            <p id="passwordCheck"></p>

                                            <div className="mb-3">
                                                <label className="label-control">Phone No.</label>
                                                <input id="phoneNo" type="number" className="form-control"
                                                    onChange={(e) => {
                                                        setPhoneNo(e.target.value)
                                                    }}
                                                />
                                            </div>

                                            <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                                            <div className="button">
                                                <button className="btn btn-primary"
                                                    onClick={signupUser}
                                                >Signup</button>
                                            </div>
                                            <div className="button">
                                                <button className="btn btn-danger"
                                                    onClick={clearInput}
                                                >Clear</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Background image */}
                </header>
                {/*Main Navigation*/}
                {/*Footer*/}
                <footer className="bg-light text-lg-start">
                    {/* Copyright */}
                    <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        © 2022 Copyright:
                        <a className="text-dark" href="http://localhost:3000/">HIRT_Project_Sunbeam</a>
                    </div>
                    {/* Copyright */}
                </footer>
                {/*Footer*/}
            </div>
        </div>
    )
}

export default Signup