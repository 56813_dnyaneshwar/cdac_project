import './index.css'
import Product from '../../components/CustomerProducts'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../../config'
import CustomerNavbar from '../../components/CustomerNavbar'

const CustomerAllProducts = () => {
  const [allProducts, setAllProducts] = useState([])
  const navigate = useNavigate()

  axios.defaults.withCredentials = true;

  useEffect(() => {
    const url = `${URL}/customer/getAllProducts`
    axios.get(url).then((response) => {
      const result = response.data
      setAllProducts(result['data'])
    })
    console.log('getting called use effect')
  }, [])

  const Logout = () => {
    const url = `${URL}/logout`
    axios.post(url).then((response) => {
      const result = response.data
      if (result['status'] === 'success') {
        navigate('/')
        toast.success(result['data'])
      } else {
        toast.error('Failed to Logout')
      }
    })
  }

  return (


      <div>
        {/*Main Navigation*/}
        <header>
          <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image15.jpg);height: 100.2vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
          <CustomerNavbar />
          {/* Background image */}
          <div id="intro" className="bg-image shadow-2-strong">
            <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
              <div className="container">
                <div className="row justify-content-center">
                  <div className="row" style={{ marginTop: '20px', marginBottom: '20px' }}>
                    {allProducts && allProducts.map((product) => {
                      return <Product product={product} />
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Background image */}
        </header>
        {/*Main Navigation*/}
      </div>

  )
}

export default CustomerAllProducts