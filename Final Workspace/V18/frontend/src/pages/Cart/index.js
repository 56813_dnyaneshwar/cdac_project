import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from 'react-toastify'
import { URL } from '../../config'
import ListCart from '../../components/listCart'
import CustomerNavbar from '../../components/CustomerNavbar'
const Cart = () => {
    const [cart, setCart] = useState([])
    const [total, setTotal] = useState('')
    axios.defaults.withCredentials = true;
    const navigate = useNavigate()

    const clearCart = () => {
        const url = `${URL}/cart/emptyAddToCart`
        axios.delete(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('cart deleted successfully')
                window.location.reload(false);
            }
            else
                toast.error('something went wrong! please try again')
        })
    }

    const buyAll = () => {
        const url = `${URL}/customer/buyAll`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                toast.success('success')
                navigate('/CustomerHome')
            }
            else
                toast.error('something went wrong! please try again')
        })
    }

    useEffect(() => {
        const url = `${URL}/cart/getCart`
        axios.get(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                setCart(result['data'].data)
                console.log(result['data'].sum)
                setTotal(result['data'].sum)
            }
            else
                toast.error('could not fetch Cart! please try again')
        })
        console.log('getting called')
    }, [])

    return (
        <div>
            {/*Main Navigation*/}
            <header>
                <style dangerouslySetInnerHTML={{ __html: "#intro {background-image: url(Photos/image15.jpg);height: 100.2vh;background-repeat: no-repeat;background-size: cover;}/* Height for devices larger than 576px */@media (min-width: 992px) {#intro {margin-top: -58.59px;}}.navbar .nav-link {color: #fff !important;}" }} />
                <CustomerNavbar />
                {/* Background image */}
                <div id="intro" className="bg-image shadow-2-strong">
                    <div className="mask d-flex align-items-center h-100" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                        <div className="container">
                            <div className="row justify-content-center">
                                {cart && cart.map((cart) => {
                                    return <ListCart cart={cart} />
                                })}
                                <div id='total'></div>
                                <div className="text-center" ><h4>Total Amount : {total}</h4></div>
                                <div className="text-center">
                                    <button id='button' className="btn btn-success" onClick={buyAll}>Buy All</button>
                                    <button id='button' className="btn btn-danger" onClick={clearCart}>Clear Cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Background image */}
            </header>
            {/*Main Navigation*/}
        </div>
    )
}

export default Cart