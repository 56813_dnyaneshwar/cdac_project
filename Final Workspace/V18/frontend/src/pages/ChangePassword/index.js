import './index.css'
import { Link } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { toast } from 'react-toastify'
import axios from 'axios'
import { useNavigate } from 'react-router'
import { URL } from '../../config'
import 'bootstrap/dist/css/bootstrap.min.css';

const ChangePassword = () => {

    const [userId, setUserId] = useState('')
    const [role, setRole] = useState('')
    const [oldPassword, setOldPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const navigate = useNavigate()

    axios.defaults.withCredentials = true;

    const ChangePasswordDetails = () => {
        if (userId !== null) {
            const body = { oldPassword, newPassword }
            const url = `${URL}/user/update`
            axios.put(url, body).then((response) => {
                const result = response.data
                if (result['status'] == 'success') {
                    toast.success('Password Updated Successfully')
                    navigate('/')
                } else {
                    toast.error('Invalid')
                }
            })
        } else
            toast.warning('Please Refresh the Page !!!')
    }

    useEffect(() => {
        const url = `${URL}/user/getUserIdRole`
        axios.get(url).then((response) => {
            const result = response.data
            console.log(result)
            setUserId(result['data'].userId)
            setRole(result['data'].role)
        })
    }, [])

    const clearInput = () => {
        document.getElementById('oldPassword').value = ""
        document.getElementById('newPassword').value = ""
    }


    return (
        <div>
            {userId &&
                <div>
                    <div className="row">
                        <h1 className='title'>Change Password</h1>
                        <div className="col"></div>
                        <div className='col'>
                            <div className="form">
                                <div className="mb-3">
                                    <label className="label-control">Old Password</label>
                                    <input id="oldPassword" type="text" className="form-control" placeholder="*********"
                                        onChange={(e) => {
                                            setOldPassword(e.target.value)
                                        }}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label className="label-control">New Password</label>
                                    <input id="newPassword" type="text" className="form-control" placeholder="*********"
                                        onChange={(e) => {
                                            setNewPassword(e.target.value)
                                        }}
                                    />
                                </div>

                                <div className="button">
                                    <button className="btn btn-primary"
                                        onClick={ChangePasswordDetails}
                                    >Change Password</button>
                                </div>
                                <div className="button">
                                    <button className="btn btn-danger"
                                        onClick={clearInput}
                                    >Clear</button>
                                </div>
                                <div className="button">
                                    <Link to={`/${role}Profile`} className="btn btn-warning">Back</Link>
                                </div>
                            </div>
                        </div>
                        <div className="col"></div>
                    </div>
                </div>
                || <div style={{ color: 'red', textAlign: 'center' }}>
                    <h1>Please Signin First</h1>
                    <div> Already Signed up? <Link to={'/'} >Signin here</Link> </div>
                </div>


            }
        </div>
    )
}

export default ChangePassword