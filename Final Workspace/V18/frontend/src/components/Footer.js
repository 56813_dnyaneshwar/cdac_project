const Footer = () => {
    return (
        <footer className="bg-light text-lg-start" style={{ position: 'fixed', bottom: 0, textAlign: 'center', width: '100%' }}>
            {/* Copyright */}
            <div className="text-center p-3" style={{ backgroundColor: 'rgba(0, 0, 0, 0)' }}>
                © 2022 Copyright : <a className="text-dark" href="http://localhost:3000/">HIRT_Project_Sunbeam</a>
            </div>
            {/* Copyright */}
        </footer>
    )
}

export default Footer