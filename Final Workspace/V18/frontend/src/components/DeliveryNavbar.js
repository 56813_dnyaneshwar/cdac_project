import { useNavigate } from 'react-router'
import axios from 'axios'
import { toast } from "react-toastify"
import { URL } from '../config'
import { Link } from 'react-router-dom'

const CustomerNavbar = () => {
    const navigate = useNavigate()
    const Logout = () => {
        const url = `${URL}/logout`
        axios.post(url).then((response) => {
            const result = response.data
            if (result['status'] === 'success') {
                navigate('/')
                toast.success(result['data'])
            } else {
                toast.error('Failed to Logout')
            }
        })
    }

    return (
        <nav className="navbar navbar-expand-lg navbar-dark d-none d-lg-block" style={{ zIndex: 2000, backgroundColor: '#808080', opacity: '0.9' }}>
            <div className="container-fluid">
                {/* Navbar brand */}
                <Link className="navbar-brand nav-link" to="/DeliveryHome">
                    <strong style={{ color: '#ffa31a' }} >HIRT | Delivery Man</strong>
                </Link>
                <button className="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarExample01" aria-controls="navbarExample01" aria-expanded="false" aria-label="Toggle navigation">
                    <i className="fas fa-bars" />
                </button>
                <div className="collapse navbar-collapse" id="navbarExample01">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item active">
                            <Link className="nav-link" aria-current="page" to="/PendingDelivery">Pending Delivery</Link>
                        </li>
                        <li className="nav-item active">
                            <Link className="nav-link" aria-current="page" to="/DeliveryProfile">Profile</Link>
                        </li>
                        <li className="nav-item active">
                            <Link to="" className="nav-link" aria-current="page" onClick={Logout} >Log Out</Link>
                        </li>

                    </ul>
                </div>
            </div>
        </nav>
    )
}

export default CustomerNavbar