const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//-----------------------------------
//      /admin/getAdminId
//-----------------------------------
router.get('/getAdminId',(request,response)=>{
    if(request.session.adminId){
        response.send(utils.createRequest(null,{ adminId : request.session.adminId}))
    } else {
        if(request.session.user){
        const userId = request.session.user.id
        const statement = `SELECT id FROM users WHERE role = 'admin'`
        const connection = db.openConnection()
            connection.query(statement,[userId],(error,result)=>{
                connection.end()
                if(result.length > 0){
                    request.session.adminId = result[0].id                
                    response.send(utils.createRequest(error,{ adminId : result[0].id}))
                }
            })
        } else {
                response.send(utils.createRequest("User Not Found"))
        }
    }
})

router.get('/getVendor',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role='vendor'`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Vendors not Found"))
        }
    })
})

router.get('/getCustomer',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role='customer'`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Customers not Found"))
        }
    })
})

router.get('/getDelivery',(request,response)=>{
    const statement =  `SELECT * FROM users WHERE role='delivery'`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("DeliveryMan not Found"))
        }
    })
})

router.post('/delete',(request,response)=>{
    const { id } = request.body
    const statement =  `DELETE FROM users WHERE id=?`
    const connection=db.openConnection()
    connection.query(statement,[id],(error,result)=>{
        connection.end()
        if(result){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest(error))
        }
    })
})

router.get('/count',(request,response)=>{
    // const { id } = request.body
    const statement =  `SELECT role, COUNT(*) as count FROM users WHERE role!='admin' GROUP BY role ORDER BY role ASC`
    const connection=db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        if(result){
            response.send(utils.createRequest(null,result))
        } else{
            response.send(utils.createRequest("Not Found"))
        }
    })
})

module.exports = router