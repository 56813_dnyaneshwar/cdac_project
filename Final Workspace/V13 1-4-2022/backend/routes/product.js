const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

//-----------------------------------
//      /product/add
//-----------------------------------
router.post('/add',(request,response)=>{
    if(request.session.vendorId){
        const vendorId = request.session.vendorId
        const { category,  type,  paymentMethod, brandName, 
            productName, maxShelfLife, moreDetails, price, unit, stock } = request.body
        const statement = `
        INSERT INTO
        products
        (vendor_id , category, type, payment_method, brand_name, product_name, max_shelf_life, more_details, price, unit, stock)
        VALUES
        ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`
        
        const connection = db.openConnection()
        connection.query(statement, 
                        [   vendorId, category,  type,  paymentMethod, brandName,
                            productName, maxShelfLife, moreDetails, price, unit, stock],
                        (error,result)=>{
                        connection.end()
                        response.send(utils.createRequest(error,result))
        })
    } else {
        error = 'vendorId Not Found'
        response.send(utils.createRequest(error))
    }

})

//-----------------------------------
//      /product/getAll
//-----------------------------------
router.get('/getAll',(request,response)=>{
    if(request.session.vendorId){

        const vendorId = request.session.vendorId
        
        const statement = `SELECT * FROM products WHERE vendor_id = ?`
    
        const connection = db.openConnection()
        connection.query(statement,[vendorId],(error,result)=>{
            connection.end()
            response.send(utils.createRequest(error,result))
        })
    } else {
        const error = 'vendorId Not Found'
        response.send(utils.createRequest(error))
    }
})

//-----------------------------------
//      /product/getById
//-----------------------------------
router.post('/getById',(request,response)=>{
    const { productId } = request.body
    
    const statement = `SELECT * FROM products WHERE id = ?`

    const connection = db.openConnection()
    connection.query(statement,[productId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

//-----------------------------------
//      /product/update
//-----------------------------------
router.put('/update',(request,response)=>{
    const { paymentMethod, maxShelfLife, moreDetails, price, unit, stock, productId } = request.body
    
    const statement = `
    UPDATE products
    SET 
    payment_method = ?, max_shelf_life = ?, more_details = ?, price = ?, unit = ?, stock = ?
    WHERE id = ?
    `
    const connection = db.openConnection()
    connection.query(statement,[paymentMethod, maxShelfLife, moreDetails, price, unit, stock, productId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})

//-----------------------------------
//      /product/delete
//-----------------------------------
router.post('/delete',(request,response)=>{
    const { productId } = request.body
    
    const statement = `DELETE FROM products WHERE id = ?`

    const connection = db.openConnection()
    connection.query(statement,[productId],(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result[0]))
    })
})

module.exports = router