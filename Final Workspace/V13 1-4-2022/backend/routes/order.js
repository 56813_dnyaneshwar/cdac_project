const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

router.post('/addOrder',(request,response)=>{
    const { orderId, customerId, vendorId, productId,productName, amount, quantity ,unit} = request.body
    const statement = `
    INSERT INTO
    order_history
    (order_id, customer_id, vendor_id, product_id, product_name, amount, quantity,unit, status)
    VALUES
    (${orderId}, ${customerId}, ${vendorId}, ${productId}, '${productName}', ${amount}, ${quantity}, '${unit}','Pending')
    `
    const connection = db.openConnection()
    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createRequest(error,result))
    })
})



module.exports = router

        // Add Order
// { 
//     "orderId" : 1,
//     "customerId" : 2,
//     "vendorId" : 1,
//     "productId" : 1,
//     "productName" : "Apple",
//     "amount" : 90,
//     "quantity" : 2 ,
//     "unit" : "kg" 
// }