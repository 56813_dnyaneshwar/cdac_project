import { toast } from 'react-toastify';
import axios from 'axios';
import { useNavigate } from 'react-router';
import { URL } from '../config'

 const Logout = () => {
     const navigate = useNavigate()
        const url = `${URL}/logout`
        axios.post(url).then((response)=>{
            const result = response.data
            if(result['status'] === 'success'){
                navigate('/')
            } else{
                toast.error('Failed to Logout')
            }
        })
    }
export default Logout